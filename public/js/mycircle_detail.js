function initDetailPage() {
    $('a.collapse-toggle').click(function(){
        $(this).text(function(i,old){
            return old=='Leggi tutto' ?  'Nascondi' : 'Leggi tutto';
        });
    });

    $(".regular").show('fast');
    $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        dots: false
    });

    $(".partecipant_slider").slick({
        dots: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 7,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1220,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 400,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 320,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

};