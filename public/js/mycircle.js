$.fn.circleTimerOld = function( options ){
    var $this = $(this), opt;
    var count = 0;

    opt = $.extend({
            "fgColor" : "#ff5f73",
            "bgColor" : "#999",
            "dateFormat" : "YYYY/MM/DD HH:mm:ss"
        }, options
    );

    //Get Dates from options
    var date_start = $(this).data('date_start');
    var date_end = $(this).data('date_end');
    var date_current = moment(new Date()).format(opt.dateFormat);
    var circleid = $(this).data('circleid');
    var thickness = $(this).data('thickness');

    //Make some calculation on dates
    var diff_start_end = moment(date_end,opt.dateFormat).diff(moment(date_start,opt.dateFormat));
    var max_seconds = moment.duration(diff_start_end).asSeconds();
    var diff_now_end = moment(date_end,opt.dateFormat).diff(moment(date_current,opt.dateFormat));
    var now_seconds = moment.duration(diff_now_end).asSeconds();

    count = now_seconds;

    $this.knob({
        'min':0,
        'max': max_seconds,
        'readOnly': true,
        'width': $this.data('width'),
        'height': $this.data('height'),
        'fgColor': opt.fgColor,
        'bgColor': opt.bgColor,
        'displayInput' : false,
        'dynamicDraw': false,
        'ticks': 0,
        'thickness': thickness
    });

    setInterval(function(){

        newVal = --count;
        $this.val(newVal).trigger('change');
        duration = moment.duration(count, 'seconds');
        $('.seconds-'+circleid).text(duration.seconds());
        $('.minutes-'+circleid).text(duration.minutes());
        $('.hours-'+circleid).text(duration.hours());
        $('.days-'+circleid).text(duration.days());
        $('.months-'+circleid).text(duration.months());

        /* DEBUG */
        if(opt.debug) {
            console.log('-------------------------------------------');
            console.log("Date Start: " + date_start);
            console.log("Date Current: " + date_current);
            console.log("Date End: " + date_end);
            console.log("Diff current and start: " + (max_seconds - count));
            console.log("Seconds left: " + count);
            console.log("Time until the end " + duration.days() + " - " + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds());
            console.log('-------------------------------------------');
        }

    }, 1000);

};

var circleTimerCounter;

$.fn.circleTimer = function( options ){
    this.circleTimerStop();
    var $this = $(this), opt;
    var count = 0;

    var color = '#ff5f73';

    var max_partecipants = $(this).data('max_partecipants');
    var current_partecipants = $(this).data('current_partecipants');
    var status = $(this).data('status');

    if(status == 'closed'){
        color = '#000';
    }

    opt = $.extend({
            "fgColor" : color,
            "bgColor" : "#a9a9a9",
            "dateFormat" : "YYYY-MM-DD HH:mm:ss"
        }, options
    );

    //Get Dates from options
    var date_start = $(this).data('date_start');
    var date_end = $(this).data('date_end');
    var date_current = moment(new Date()).format(opt.dateFormat);
    var circleid = $(this).data('circleid');
    var thickness = $(this).data('thickness');

    //Make some calculation on dates
    var diff_start_end = moment(date_end,opt.dateFormat).diff(moment(date_start,opt.dateFormat));
    var max_seconds = moment.duration(diff_start_end).asSeconds();
    var diff_now_end = moment(date_end,opt.dateFormat).diff(moment(date_current,opt.dateFormat));
    var now_seconds = moment.duration(diff_now_end).asSeconds();

    count = now_seconds;

    $this.knob({
        'min':0,
        'max': max_partecipants,
        'readOnly': true,
        'width': $this.data('width'),
        'height': $this.data('height'),
        'fgColor': opt.fgColor,
        'bgColor': opt.bgColor,
        'displayInput' : false,
        'dynamicDraw': false,
        'ticks': 0,
        'thickness': thickness
    });

    $this.val(current_partecipants).trigger('change');

    if(typeof date_start === 'undefined' || typeof date_end === 'undefined') {
        return;
    }

    circleTimerCounter = window.setInterval(function(){
        //console.log('start');
        newVal = --count;
        //$this.val(newVal).trigger('change');
        duration = moment.duration(count, 'seconds');
        $('.seconds-'+circleid).text(duration.seconds());
        $('.minutes-'+circleid).text(duration.minutes());
        $('.hours-'+circleid).text(duration.hours());
        $('.days-'+circleid).text(duration.days());
        $('.months-'+circleid).text(duration.months());

        /* DEBUG */
        if(opt.debug) {
            console.log('-------------------------------------------');
            console.log("Date Start: " + date_start);
            console.log("Date Current: " + date_current);
            console.log("Date End: " + date_end);
            console.log("Diff current and start: " + (max_seconds - count));
            console.log("Seconds left: " + count);
            console.log("Time until the end " + duration.days() + " - " + duration.hours() + ":" + duration.minutes() + ":" + duration.seconds());
            console.log('-------------------------------------------');
        }

    }, 1000);

};

$.fn.circleTimerStop = function(){
    //console.log('stop');
    for (var i = 1; i < circleTimerCounter; i++)
        window.clearInterval(i);
}

$.fn.stars = function( options ){
    var $this = $(this);
    var stars = Array.from(($this.data('stars')));

    $.each(stars,function(i,v){
        if(v == "1"){
            $this.append('<i class="fas fa-star mr-5"></i>');
        }
        else if(v == "0.5"){
            $this.append('<i class="fas fa-star-half-alt mr-5"></i>');
        }
        else if(v == "0"){
            $this.append('<i class="far fa-star mr-5"></i>');
        }
    });
};

function initCirclesSlider() {

    $(".regular-circle").show('fast');
    $(".regular-circle").slick({
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        variableWidth: true,
        dots: false
    });

};

$(document).on('click', '.mobile-menu-toggle', function (e) {
    e.preventDefault();
    $(this).parent().find('.mobile-submenu').toggle();
});