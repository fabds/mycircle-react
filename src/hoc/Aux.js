/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const Aux = (props) => {
    return props.children;
};

export default Aux;