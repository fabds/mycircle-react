import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const SectionContainer = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className={"col-sm-12 "+props.class}>
                    {props.children}
                </div>
            </div>
        </div>
    );
}

export default SectionContainer;