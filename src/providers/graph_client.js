import {ApolloClient, HttpLink, InMemoryCache} from 'apollo-boost';
import {createHttpLink} from "apollo-link-http";

const defaultOptions = {
    watchQuery: {
        fetchPolicy: 'network-only',
        errorPolicy: 'ignore',
    },
    query: {
        fetchPolicy: 'network-only',
        errorPolicy: 'all',
    },
}
const httpLink = createHttpLink({
    uri: process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GRAPH_PATH
});

/**
 *
 * @type {ApolloClient<NormalizedCacheObject>}
 */
const graph_client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
    defaultOptions: defaultOptions,
    fetchOptions: {
        mode: 'no-cors'
    }
});

export default graph_client