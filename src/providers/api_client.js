import axios from "axios";

/**
 *
 * @type {AxiosStatic.create}
 */
const api_client = new axios.create({
    baseURL: process.env.REACT_APP_API_URL+'/rest/V1',
    timeout: 5000,
    headers: {'Content-Type': 'application/json'}
});

export default api_client