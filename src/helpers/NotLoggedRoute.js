import React from "react";
import { Route, Redirect } from 'react-router-dom';

/**
 * 
 * @param Component
 * @param authed
 * @param rest
 * @returns {*}
 * @constructor
 */
const NotLoggedRoute = ({ component: Component, authed, ...rest }) => (
    <Route {...rest} render={(props) => (
        !authed ? <Component {...props} /> : <Redirect to='/' />
    )} />
);

export default NotLoggedRoute;