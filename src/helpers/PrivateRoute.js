import React, {Component} from "react";
import { Route, Redirect, withRouter } from 'react-router-dom';

class PrivateRoute extends Component {

    /**
     *
     * @param prevProps
     */
    componentDidUpdate(prevProps) {
        //if (this.props.path === this.props.location.pathname && this.props.location.pathname !== prevProps.location.pathname) {
        window.scrollTo(0, 0)
        //}
    }

    /**
     *
     * @returns {*}
     */
    render() {
        const { component: Component, ...rest } = this.props;

        return <Route {...rest} render={(props) => (
            this.props.authed ? <Component {...props} /> : <Redirect to='/login' />
        )} />;
    }
}


export default withRouter(PrivateRoute);