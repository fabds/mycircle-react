export default {
    //General
    "appname" : "MyCircle",
    "register" : "Registrati",
    "login" : "Accedi",
    "logout" : "Esci",
    "account" : "Profilo",
    "wishlist" : "Wishlist",
    "search" : "Cerca",
    "subscribe_to_newsletter" : "Iscriviti alla Newsletter",
    "subscribe" : "Iscriviti",
    "all_right_reserved" : "Tutti i diritti riservati ",
    "all_cities" : "Tutte le città",
    "open_circle" : "Apri un cerchio",
    "circles_number" : "Cerchi Aperti {count}",
    "circle" : "Cerchia",
    "circles" : "Cerchie",
    "active" : "Attiva",
    "inactive" : "Non Attiva",
    "status" : "Stato",
    "open" : "Aperta",
    "closed" : "Chiusa",
    "partecipants" : "Partecipanti",

    //Time and dates
    "time.months": "Mesi",
    "time.days": "Giorni",
    "time.hours": "Ore",
    "time.minutes": "Minuti",
    "time.seconds": "Secondi",


    //Banner Content
    "banner_content.slide1.text" : "Natale è ancora più magico a Disney Land Paris",
    "banner_content.slide1.button1" : "Crea una Cerchia",
    "banner_content.slide1.button2" : "Partecipa ad una Cerchia",

    "nav.experiences" : "Esperienze",
    "nav.howitworks" : "Come funziona",
    "nav.circles" : "Cerchie",
    "nav.packages" : "Pacchetti",
    "nav.aboutus" : "Chi siamo",
    "nav.contacts" : "Contatti",
    "nav.community" : "Comunità"
}