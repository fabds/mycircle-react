import it from "./it/it";
import en from "./en/en";

export default {
    en : en,
    it : it
}