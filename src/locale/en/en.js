export default {
    //General
    "appname" : "MyCircle",
    "register" : "Register",
    "login" : "Login",
    "logout" : "Logout",
    "account" : "Account",
    "wishlist" : "Wishlist",
    "search" : "Search",
    "subscribe_to_newsletter" : "Subscribe to newsletter",
    "subscribe" : "Subscribe",
    "all_right_reserved" : "All rights reserved ",
    "all_cities" : "All cities",
    "open_circle" : "Open a circle",
    "circles_number" : "Open Circles {count}",
    "circle" : "Circle",
    "circles" : "Circles",
    "active" : "Active",
    "inactive" : "Inactive",
    "status" : "Status",
    "open" : "Open",
    "closed" : "Closed",
    "partecipants" : "Partecipants",

    //Time and dates
    "time.months": "Months",
    "time.days": "Days",
    "time.hours": "Hours",
    "time.minutes": "Minutes",
    "time.seconds": "Seconds",

    //Banner Content
    "banner_content.slide1.text" : "Christmas in Disney Land Paris",
    "banner_content.slide1.button1" : "Create a Circle",
    "banner_content.slide1.button2" : "Subscribe to a Circle",

    "nav.experiences" : "Experiences",
    "nav.howitworks" : "How it works",
    "nav.circles" : "Circles",
    "nav.packages" : "Packages",
    "nav.aboutus" : "About Us",
    "nav.contacts" : "Contacts",
    "nav.community" : "Community"
}