import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { sessionService, sessionReducer } from 'redux-react-session';
import { addLocaleData } from "react-intl";
import en from "react-intl/locale-data/en";
import it from "react-intl/locale-data/it";

import './assets/css/bootstrap.css';
import './assets/css/linearicons.css';
import './assets/css/magnific-popup.css';
import './assets/css/jquery-ui.css';
import './assets/css/nice-select.css';
import './assets/css/animate.min.css';
//import './assets/css/main.css';
import './assets/css/circles.css';
import './assets/css/slick.css';
import './assets/css/slick-theme.css';

import App from './App';
import CircleStats from './store/reducers/circles';
import Categories from './store/reducers/categories';
import Locale from './store/reducers/locale';
import Checkoutdata from './store/reducers/cart';
import graph_client from './providers/graph_client';
import { setLocale } from './store/actions/Locale';

import * as serviceWorker from './serviceWorker';
import detectBrowserLanguage from 'detect-browser-language'
import {createHttpLink} from "apollo-link-http";

addLocaleData(en);
addLocaleData(it);

/**
 *
 * @type {*|((() => <R>(a: R) => R) | <F extends Function>(f: F) => F | <A, R>(f1: (b: A) => R, f2: Func0<A>) => Func0<R> | <A, T1, R>(f1: (b: A) => R, f2: Func1<T1, A>) => Func1<T1, R> | <A, T1, T2, R>(f1: (b: A) => R, f2: Func2<T1, T2, A>) => Func2<T1, T2, R> | <A, T1, T2, T3, R>(f1: (b: A) => R, f2: Func3<T1, T2, T3, A>) => Func3<T1, T2, T3, R> | <A, B, R>(f1: (b: B) => R, f2: (a: A) => B, f3: Func0<A>) => Func0<R> | <A, B, T1, R>(f1: (b: B) => R, f2: (a: A) => B, f3: Func1<T1, A>) => Func1<T1, R> | <A, B, T1, T2, R>(f1: (b: B) => R, f2: (a: A) => B, f3: Func2<T1, T2, A>) => Func2<T1, T2, R> | <A, B, T1, T2, T3, R>(f1: (b: B) => R, f2: (a: A) => B, f3: Func3<T1, T2, T3, A>) => Func3<T1, T2, T3, R> | <A, B, C, R>(f1: (b: C) => R, f2: (a: B) => C, f3: (a: A) => B, f4: Func0<A>) => Func0<R> | <A, B, C, T1, R>(f1: (b: C) => R, f2: (a: B) => C, f3: (a: A) => B, f4: Func1<T1, A>) => Func1<T1, R> | <A, B, C, T1, T2, R>(f1: (b: C) => R, f2: (a: B) => C, f3: (a: A) => B, f4: Func2<T1, T2, A>) => Func2<T1, T2, R> | <A, B, C, T1, T2, T3, R>(f1: (b: C) => R, f2: (a: B) => C, f3: (a: A) => B, f4: Func3<T1, T2, T3, A>) => Func3<T1, T2, T3, R> | <R>(f1: (b: any) => R, ...funcs: Function[]) => (...args: any[]) => R | <R>(...funcs: Function[]) => (...args: any[]) => R)}
 */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Add Reducers
 * @type {Reducer<any> | Reducer<any, AnyAction>}
 */
const reducer = combineReducers({
    session: sessionReducer,
    circles_stats: CircleStats,
    categories: Categories,
    locale: Locale,
    checkout_data: Checkoutdata
});

/**
 *
 * @type {{circles_stats: {date: null, city: null, category: string}, categories: Array, lang: string}}
 */
const initialState = {
    circles_stats : {
        date: null,
        city: null,
        category: "2"
    },
    categories: [],
    lang: 'it'
};

/**
 *
 * @param session
 * @returns {boolean}
 */
const validateSession = (session) => {
    // check if your session is still valid
    return true;
};

/**
 *
 * @type {{refreshOnCheckAuth: boolean, driver: string, validateSession: (function(*): boolean)}}
 */
const options = { refreshOnCheckAuth: true, driver: 'COOKIES', validateSession };

/**
 *
 * @type {*}
 */
const store = createStore(reducer, initialState, compose(composeEnhancers(
    applyMiddleware(thunkMiddleware)
)));


// if(localStorage.myCircleLang){
//     store.dispatch(setLocale(localStorage.myCircleLang));
// }

// Init the session service
sessionService.initSessionService(store, options);

let browser_language = detectBrowserLanguage();

let url = new URL(window.location.href);
let path = url.pathname.split('/');
let language = (path[1] === 'it' | path[1] === 'en') ? path[1] : '';
if (language !== '')
    store.dispatch(setLocale(language));
else
    window.location.href = (browser_language === 'it' || browser_language === 'en') ? '/'+browser_language+'/' : '/en/';

const httpLink = createHttpLink({
    headers: {
        store: language
    },
    uri: process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GRAPH_PATH
});

graph_client.link = httpLink;

const app = (
    <Provider store={store}>
        <BrowserRouter  basename={language}>
            <ApolloProvider client={graph_client}>
                <App />
            </ApolloProvider>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
