import React, {Component} from 'react';
import {Switch, withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import './App.css';
import { userActions } from './services/Users';
import Layout from './components/Layout/Layout';
import PrivateRoute from "./helpers/PrivateRoute";
import ScrollRoute from "./helpers/ScrollRoute";

//Containers
import Home from "./containers/Home/Home";
import Contact from "./containers/Contact/Contact";
import HowItWorks from "./containers/HowItWorks/HowItWorks";
import Circles from "./containers/Circles/Circles";
import Packages from "./containers/Packages/Packages";
import AboutUs from "./containers/AboutUs/AboutUs";
import Experiences from "./containers/Experiences/Experiences";
import Detail from "./containers/Detail/Detail";
import Login from "./containers/Login/Login";
import Register from "./containers/Login/Register";
import NotFound from "./containers/NotFound/NotFound";
import Account from "./containers/Account/Account";
import Listing from "./containers/Listing/Listing";
import Cart from "./containers/Cart/Cart";
import Checkout from "./containers/Checkout/Checkout";
import Wishlist from "./containers/Wishlist/Wishlist";
import Page from "./containers/Page/Page";
import ExperiencesMain from "./containers/Experiences/ExperiencesMain";
import { IntlProvider } from "react-intl"
import messages from "./locale/dictionary"
import {StripeProvider} from "react-stripe-elements";
import {sessionService} from "redux-react-session";
import {setCartdata} from "./store/actions/Cart";

class App extends Component {

    constructor() {
        super();
        this.state = {stripe: null};

    }

    componentDidMount() {
        if (window.Stripe) {
            this.setState({stripe: window.Stripe('pk_test_J0NMmUVULQJFWaYIX0S32hJ0')});
        } else {
            document.querySelector('#stripe-js').addEventListener('load', () => {
                // Create Stripe instance once Stripe.js loads
                this.setState({stripe: window.Stripe('pk_test_J0NMmUVULQJFWaYIX0S32hJ0')});
            });
        }

    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        userActions.getUserdata().then(user_data => {

                if (!user_data) {
                    userActions.logout();
                }
                else {
                   return userActions.getCartdata();
                }

            })
            .then(cart_data => {
                this.props.setCartdata(cart_data);
            });


    }


    render() {

        return (
            <StripeProvider stripe={this.state.stripe}>
                <IntlProvider key={ this.props.lang } locale={this.props.lang} messages={messages[this.props.lang]}>
                        <Layout>
                            <Switch >
                                {/* Public Access */}
                                <ScrollRoute exact path="/" component={Home}/>
                                <ScrollRoute path="/how-it-works" component={HowItWorks}/>
                                <ScrollRoute exact path="/experiences" component={Experiences}/>
                                <ScrollRoute path="/cerchi/:category+" component={ExperiencesMain}/>
                                <ScrollRoute path="/circles" component={Circles} />
                                <ScrollRoute path="/packages" component={Packages}/>
                                <ScrollRoute path="/about-us" component={AboutUs}/>
                                <ScrollRoute path="/contact" component={Contact}/>
                                <ScrollRoute path="/listing" component={Listing}/>
                                <ScrollRoute path="/cart" component={Cart}/>
                                <ScrollRoute path="/wishlist" component={Wishlist}/>
                                <ScrollRoute path="/detail/:slug" component={Detail}/>
                                <ScrollRoute path="/page/:slug" component={Page}/>
                                <ScrollRoute path="/login" component={Login} authed={this.props.authenticated}/>
                                <ScrollRoute path="/register" component={Register} authed={this.props.authenticated}/>
                                <PrivateRoute path="/account" component={Account} authed={this.props.authenticated}/>
                                <PrivateRoute path="/checkout" component={Checkout} authed={this.props.authenticated}/>

                                <ScrollRoute path="*" component={NotFound}/>
                            </Switch>
                        </Layout>
                </IntlProvider>
            </StripeProvider>
        );
    }
}

const mapState = (state) => ({
    checked: state.session.checked,
    authenticated: state.session.authenticated,
    lang: state.locale.lang
});

function mapDispatchToProps(dispatch) {
    return {
        setCartdata: cart_data => dispatch(setCartdata(cart_data)),
    };
};

export default withRouter(connect(mapState, mapDispatchToProps)(App));
