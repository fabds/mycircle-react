import React, {Component} from "react";
import { connect } from 'react-redux';
import { setDate,setCity,setCategory } from "../../../store/actions/Circles";
import {CITY} from "../../../graphql/attributes";
import { Query } from 'react-apollo';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {CATEGORIES} from "../../../graphql/categories";
import { FormattedMessage } from "react-intl";


class CircleSearchFull extends Component{

    constructor(props) {
        super(props);

        this.state = this.props.circles_stats;

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handlecalendarChange = this.handlecalendarChange.bind(this);
    }

    componentDidMount() {

        this.state.category = this.props.current_category;
        this.props.setCategory(this.state.category);

    }

    handlecalendarChange(date) {
        this.setState({
            date: date
        });

    }


    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        const { city, date, category } = this.state;

        this.props.setCity(city);
        this.props.setDate(date);
        this.props.setCategory(category);
    };

    render() {
        const now = new Date();
        var next = new Date();
        next.setMonth(now.getMonth()+12);

        var filter_city = (typeof this.props.filters) ? this.props.filters.find(o => o.request_var === 'city') : [];
        var filter_category = (typeof this.props.filters) ? this.props.filters.find(o => o.request_var === 'cat') : [];
        return (
        <div className="container">
            <form id="search-form"  method="post" onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="col-md-3">
                        <select name={filter_city.request_var} className="form-control  form-control-lg rounded-0" value={(this.state.city) ? this.state.city : ""} onChange={this.handleChange}>
                            <option value="">Tutte le città</option>
                            {filter_city.filter_items.map((attribute, i) => {
                                console.log(attribute.items_count);
                                return (<option disabled={(attribute.items_count == "0") ? "true" : false} key={attribute.value_string} value={attribute.value_string}>{attribute.label}</option>)
                            })}
                        </select>
                    </div>

                    <div className="col-md-3">
                        <select name="category" className="form-control  form-control-lg rounded-0"
                                onChange={this.handleChange} value={this.state.category}>
                            {
                                filter_category.filter_items.map(cat => (
                                    <option key={cat.value_string} value={cat.value_string}>{cat.label}</option>)
                                )
                            }
                        </select>

                    </div>

                    <div className="col-md-3">
                        <DatePicker className="form-control  form-control-lg rounded-0"  placeholder="Seleziona data"
                                    selected={this.state.date}
                                    dateFormat="dd/MM/yyyy"
                                    onChange={this.handlecalendarChange}
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    minDate={now}
                                    maxDate={next}
                                    dropdownMode="select"
                        />
                        {/*<input type="text" readOnly className="form-control form-control-lg rounded-0 bg-white"*/}
                               {/*onChange={this.handleChange} id="date" name="date" placeholder="Seleziona data" required/>*/}
                    </div>
                    <div className="col-md-3">
                        <button className="rounded-0 btn btn-primary btn-lg btn-block">
                            <FormattedMessage id="search" defaultMessage="Cerca"/>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        )
    }
}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{circles_stats: ({date: Date, city: string, category: string}|Listing.state.circles_stats|{date, city, category}|{date: null, city: null, category: string}|initialState.circles_stats|ExperiencesList.state.circles_stats)}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        circles_stats: state.circles_stats
    }
};

/**
 *
 * @param dispatch
 * @returns {{setDate: (function(*=): *), setCity: (function(*=): *), setCategory: (function(*=): *)}}
 */
function mapDispatchToProps(dispatch) {
    return {
        setDate: date => dispatch(setDate(date)),
        setCity: city => dispatch(setCity(city)),
        setCategory: category => dispatch(setCategory(category))
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(CircleSearchFull);
