import React from "react";
import Moment from "react-moment";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function CircleDate(props) {

    if (props.circle.date_start !== null) {
        return <div className="date-row">dal <Moment format="DD/MM/YYYY">{props.circle.date_start}</Moment> al <Moment format="DD/MM/YYYY">{props.circle.date_end}</Moment></div>;
    }
    else if (props.circle.date_start === null && props.circle.circle_date !== null) {
        return <div className="date-row">il <Moment format="DD/MM/YYYY">{props.circle.circle_date}</Moment></div>;
    }
    return <div className="date-row">dal <Moment format="DD/MM/YYYY">{new Date()}</Moment></div>;
}

export default CircleDate;