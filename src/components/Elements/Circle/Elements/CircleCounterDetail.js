import React from "react";
import { FormattedMessage } from "react-intl";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function CircleCounterDetail(props) {

    if (props.circle.circles.circles_count === 1) {
        return  (
            <div className="countdown-row text-right countdown-detail time-detail">
                <div className="countdown-container text-center">
                    <p className={"countdown-counter-item countdown-item months-" + props.circle.id}>-</p>
                    <p className="countdown-label"><FormattedMessage id="time.months" defaultMessage="Mesi"/></p>
                </div>
                <div className="countdown-container text-center">
                    <p className={"countdown-counter-item countdown-item days-" + props.circle.id}>-</p>
                    <p className="countdown-label"><FormattedMessage id="time.days" defaultMessage="Giorni"/></p>
                </div>
                <div className="countdown-container text-center">
                    <p className={"countdown-counter-item countdown-item hours-" + props.circle.id}>-</p>
                    <p className="countdown-label"><FormattedMessage id="time.hours" defaultMessage="Ore"/></p>
                </div>
                <div className="countdown-container text-center">
                    <p className={"countdown-counter-item countdown-item minutes-" + props.circle.id}>-</p>
                    <p className="countdown-label"><FormattedMessage id="time.minutes" defaultMessage="Minuti"/></p>
                </div>
            </div>

        )
        }


    else {
        return (
            <div className="countdown-row text-right countdown-detail time-detail">

            </div>
        )
    }
}

export default CircleCounterDetail;