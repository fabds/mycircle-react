import React from "react";
import { FormattedMessage } from "react-intl";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
function CircleCounter(props) {

    if (props.circle.circles.circles_count === 1) {
        return  (
            <div className="countdown-row">
                <div className="countdown-container">
                    <p className={"countdown-item months-" + props.circle.id}>-</p>
                    <p className="countdown-label">mesi</p>
                </div>
                <div className="countdown-container">
                    <p className={"countdown-item days-" + props.circle.id}>-</p>
                    <p className="countdown-label">giorni</p>
                </div>
                <div className="countdown-container">
                    <p className={"countdown-item hours-" + props.circle.id}>-</p>
                    <p className="countdown-label">ore</p>
                </div>
                <div className="countdown-container">
                    <p className={"countdown-item minutes-" + props.circle.id}>-</p>
                    <p className="countdown-label">minuti</p>
                </div>
                <div className="countdown-container">
                    <p className={"countdown-item seconds-" + props.circle.id}>-</p>
                    <p className="countdown-label">secondi</p>
                </div>
            </div>
        )
    }
    else if (props.circle.circles.circles_count >= 1) {
        return (
            <div className="countdown-row circle_message">
                <FormattedMessage id="circles_number"
                                  class="circle_open"
                                  defaultMessage="Cerchi Aperti {count}"
                                  values={{ count: props.circle.circles.circles_count  }}/>
            </div>
        )
    }
    else {
        return (
            <div className="countdown-row circle_message">
                <FormattedMessage id="open_circle"
                                  defaultMessage="Apri un cerchio"/>
            </div>
        )
    }

}

export default CircleCounter;