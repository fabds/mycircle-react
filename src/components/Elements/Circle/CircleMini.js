import React from "react"
import { FormattedMessage } from "react-intl";
import CircleCounterDetail from "./Elements/CircleCounterDetail";
import {Link} from "react-router-dom";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleMini = (props) => {
    /**
     *
     * @type {{background: string, height: string, display: string, color: string, textAlign: string, fontWeight: string, fontSize: string, padding: string}}
     */
    const styleElement = {background: "rgb(224, 94, 132)", 
    height: "500px", 
    display: "block", 
    color: "#FFF", 
    textAlign: "center", 
    fontWeight: "400", 
    fontSize: "15px", 
    padding: "25px 10px"};

    let circle_active = 0;
    (typeof(props.circle_data.status) !== "undefined" && props.circle_data.status !== '') ? circle_active = props.circle_data.status : circle_active = 1;

    let circle_status = 0;
    (props.circle_data.max_partecipants === props.circle_data.guest_count)?circle_status=0:circle_status=1;

    return (
        <div className="row">
            <div className="col-xl-4  col-md-4  col-12">
                <CircleCounterDetail circle={props.circle_data} />
            </div>
            <div className="col-xl-4  col-md-4  col-12">
                <div className="circle-text-container small-circle-container">
                    <Link to={"/detail/" + props.circle_data.sku}>
                        <div
                            key={props.circle_data.id}
                            className="countdown"
                            data-width="250"
                            data-height="250"
                            data-date_start={(typeof props.circle_data.circles.circles_data[0] !== 'undefined')?props.circle_data.circles.circles_data[0].open_date:null}
                            data-date_end={(typeof props.circle_data.circles.circles_data[0] !== 'undefined')?props.circle_data.circles.circles_data[0].end_date:null}
                            data-current_partecipants={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].current_partecipants !== '') ? props.circle_data.circles.circles_data[0].current_partecipants : '0'}
                            data-max_partecipants={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].group_max_number !== '') ? props.circle_data.circles.circles_data[0].group_max_number : '0'}
                            data-status={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].status !== '') ? props.circle_data.circles.circles_data[0].status : 'open'}
                            data-thickness={(typeof(props.thickness) !== "undefined") ? props.thickness : .05}
                            data-circleid={props.circle_data.id}>
                        </div>
                    </Link>
                    <div className="circle-text circle-text-top small-circle">
                        <div className="circle__inner">
                            <div className="circle__wrapper">
                                <div className="circle_background">
                                    <div className="circle_mask">
                                        <div style={styleElement}>
                                            <FormattedMessage id="circle" defaultMessage="Cerchia"/>: {(circle_active)?<FormattedMessage id="active" defaultMessage="Attiva"/>:<FormattedMessage id="inactive" defaultMessage="Non Attiva"/>}<br/>
                                            <FormattedMessage id="status" defaultMessage="Stato"/>: {(circle_status)?<FormattedMessage id="open" defaultMessage="Aperta"/>:<FormattedMessage id="closed" defaultMessage="Chiusa"/>}<br/>
                                            <FormattedMessage id="partecipants" defaultMessage="Partecipanti"/>: {(typeof(props.circle_data.guest_count) !== 'undefined' && props.circle_data.guest_count !== '')?props.circle_data.guest_count:0}/{props.circle_data.max_partecipants}
                                        </div>
                                    </div>
                                </div>
                                <div className="stars text-warning">
                                    <div className="stars-elements" data-stars={(typeof(props.circle_data.stars) !== "undefined") ? props.circle_data.stars : ["0","0","0","0","0"]}></div>
                                </div>
                                <div className="circle__content circle-separator">
                                    <div className="price-box">
                                        <span className="old-price"><s>{props.circle_data.price.regularPrice.amount.value} €</s></span>
                                        <span className="separator-price">|</span>
                                        <span className="new-price">{props.circle_data.price.minimalPrice.amount.value} €</span>
                                    </div>
                                    <div className="join-box">
                                        <span className="join-item">
                                            <a href="details" tabIndex="0">
                                                <i className="fa fa-arrow-circle-right fa-3x"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-xl-4 col-md-4 col-12">
                <div className="countdown-row text-left countdown-detail social-detail">
                    <div className="countdown-container text-center">
                        <a href="/" className="countdown-item d-block"><i className="fa fa-envelope"></i></a>
                        <p className="countdown-label">&nbsp;</p>
                    </div>
                    <div className="countdown-container text-center">
                        <a href="/" className="countdown-item d-block"><i className="fa fa-share-alt"></i></a>
                        <p className="countdown-label">&nbsp;</p>
                    </div>
                    <div className="countdown-container text-center">
                        <a href="/" className="countdown-item d-block"><i className="fa fa-plus"></i></a>
                        <p className="countdown-label">&nbsp;</p>
                    </div>
                    <div className="countdown-container text-center">
                        <a href="/" className="countdown-item d-block"><i className="fa fa-heart"></i></a>
                        <p className="countdown-label">&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default CircleMini