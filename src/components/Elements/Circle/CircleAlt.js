import React from "react";
import {Link} from "react-router-dom";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleAlt = (props) => {
    return (
        <div className="text-center">
            <div className="circle-text-container circle-smaller">
                <div className="circle-text circle-text-top">
                    <div className="circle__inner">
                        <div className="circle__wrapper">
                            <div className="circle_background">
                                <div className="circle_mask">
                                    <img src={props.circle_data.image.url} alt={props.circle_data.name} />
                                </div>
                            </div>
                            <div className="circle__content circle-separator">
                                <div className="circle-main-details">
                                    <div className="title-row">{props.circle_data.name.substring(0, 20)}</div>
                                    {props.circle_data.circle_name}
                                </div>
                                <hr className="circle-separator" />
                                <div className="price-box">
                                    <h3 className="old-price-text">
                                        <del className="old-price text">{props.circle_data.price.regularPrice.amount.value} €</del>
                                    </h3>
                                    <h2 className="new-price">{props.circle_data.price.minimalPrice.amount.value} €</h2>
                                </div>
                                <div className="join-box">
                                    <span className="join-item">
                                        <Link to={"/detail/" + props.circle_data.sku}>
                                            <i className="fa fa-arrow-circle-right fa-3x"></i>
                                        </Link>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
        </div>
    )
};

export default CircleAlt