import React from "react";
import {Link} from "react-router-dom";
import CircleDate from './Elements/CircleDate';
import CircleCounter from './Elements/CircleCounter';

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 * <div className={((typeof(props.grid_class) !== "undefined" && props.grid_class !== '') ? props.grid_class : "col-lg-4 col-md-6 col-sm-6") + " text-center"}>
 */
const Circle = (props) => (
        <div className="paddingl10 mask-height-500">
        <div className="circle-text-container text-center affix">
            <Link to={"/detail/" + props.circle_data.sku}>
                <div
                    key={props.circle_data.id}
                    className="countdown"
                    data-width="476"
                    data-height="476"
                    data-date_start={(typeof props.circle_data.circles.circles_data[0] !== 'undefined')?props.circle_data.circles.circles_data[0].open_date:null}
                    data-date_end={(typeof props.circle_data.circles.circles_data[0] !== 'undefined')?props.circle_data.circles.circles_data[0].end_date:null}
                    data-current_partecipants={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].current_partecipants !== '') ? props.circle_data.circles.circles_data[0].current_partecipants : '0'}
                    data-max_partecipants={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].group_max_number !== '') ? props.circle_data.circles.circles_data[0].group_max_number : '0'}
                    data-status={(typeof props.circle_data.circles.circles_data[0] !== 'undefined' && props.circle_data.circles.circles_data[0].status !== '') ? props.circle_data.circles.circles_data[0].status : 'open'}
                    data-thickness={(typeof(props.thickness) !== "undefined") ? props.thickness : .05}
                    data-circleid={props.circle_data.id}>
                </div>
            </Link>
            <div className="circle-text circle-text-top">
                <div className="circle__inner">
                    <div className="circle__wrapper">
                        <div className="circle_background">
                            <div className="circle_mask">
                                <img src={props.circle_data.image.url} alt={props.circle_data.name} />
                            </div>
                        </div>

                        <div className="stars text-warning">
                            <div className="stars-elements" data-stars={(typeof(props.circle_data.stars) !== "undefined") ? props.circle_data.stars : ["0","0","0","0","0"]}></div>
                        </div>
                        <div className="circle__content circle-separator">
                            <div className="circle-main-details">
                                <div className="title-row">{props.circle_data.name.substring(0, 20)}</div>
                                {props.circle_data.circle_name}
                                <CircleDate circle={props.circle_data} />
                                <CircleCounter circle={props.circle_data} />
                            </div>
                            <hr className="circle-separator" />
                            <div className="price-box">
                                <span className="old-price"><s>{props.circle_data.price.regularPrice.amount.value} €</s></span>
                                <span className="separator-price">|</span>
                                <span className="new-price">{props.circle_data.price.minimalPrice.amount.value} €</span>
                            </div>
                            <div className="join-box">
                                <span className="join-item">
                                    <Link to={"/detail/" + props.circle_data.sku}>
                                        <i className="fa fa-arrow-circle-right fa-3x"></i>
                                    </Link>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default Circle;

