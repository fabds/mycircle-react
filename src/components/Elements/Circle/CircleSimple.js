import React from 'react';

import Circle from "./Circle";
import CircleMini from "./CircleMini";
import CircleAlt from "./CircleAlt";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleSimple = (props) => {
    return (props.type === 'mini')
    ?
    <CircleMini
        circle_data={props.circle}
        key={"mini-"+props.circle.id}
        grid_class={props.grid_class} />
    : (props.type === 'alt') ?
            <CircleAlt
                circle_data={props.circle}
                key={"alt-"+props.circle.id}
                grid_class={props.grid_class}
            /> :
            <Circle
                circle_data={props.circle}
                key={'full-'+props.circle.id}
                grid_class={props.grid_class} />;
};

export default CircleSimple;