import React, {Component} from 'react';
import {connect} from 'react-redux';

import Menu from "./Elements/Menu";

import './Header.css';
import HeaderSwitcher from "./Elements/HeaderSwitcher";

class Header extends Component {

    render() {
        return (
            <header id="header">
                <nav className="main-menu">
                    <HeaderSwitcher properties={this.props}/>
                </nav>
                {/* Main Menu Block */}
                <Menu/>
            </header>
        );
    }
}

// user: state.user,
//     authenticated: state.authenticated

const mapState = (state) => ({
    user: state.session.user,
    authenticated: state.session.authenticated
});

export default connect(mapState)(Header);
