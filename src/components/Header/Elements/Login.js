import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu} from 'reactstrap';
import {Link} from "react-router-dom";
import { FormattedMessage } from "react-intl";

class Login extends Component {

    constructor(props) {
        super(props);

        this.rightToggle = this.rightToggle.bind(this);
        this.state = {
            rightDropdownOpen: false
        };
    }

    rightToggle() {
        this.setState({
            rightDropdownOpen: !this.state.rightDropdownOpen
        });
    }

    render()
    {
        return (
            <ButtonDropdown isOpen={this.state.rightDropdownOpen} toggle={this.rightToggle}>
                <DropdownToggle color="flat" caret>
                    <span className="text-muted" style={{"fontSize": "20px"}}><FormattedMessage id="login" defaultMessage="Accedi"/>&nbsp;&nbsp;</span>
                </DropdownToggle>
                <DropdownMenu>
                    <Link className="dropdown-item" to="/register"><FormattedMessage id="register" defaultMessage="Registrati"/></Link>
                    <Link className="dropdown-item" to="/login"><FormattedMessage id="login" defaultMessage="Accedi"/></Link>
                </DropdownMenu>
            </ButtonDropdown>
        )
    }
}

export default Login