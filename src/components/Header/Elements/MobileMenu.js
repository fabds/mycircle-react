import React, {PureComponent} from 'react';
import MainMenuList from './Menu/MainMenuList';
import {Link} from 'react-router-dom';
import {slide as Menu} from 'react-burger-menu'
import Language from './Language';

class MobileMenu extends PureComponent {

    render() {
        return (

            <div className="col-2 d-block d-lg-none">
                <nav>
                    <Menu>

                        <ul className="mobile-menu">
                            <li>
                                <div className="row">
                                    <div className="col-4">
                                        <Link to="/account" className="btn btn-flat">
                                            <i className="fa fa-2x fa-user text-muted"
                                               style={{"fontSize": "30px"}}></i>
                                        </Link>
                                    </div>
                                    <div className="col-4">
                                        <Link to="/wishlist" className="btn btn-flat">
                                            <i className="fa fa-2x fa-heart text-muted"
                                               style={{"fontSize": "30px"}}></i>
                                        </Link>
                                    </div>
                                    <div className="col-4">
                                        <Link to="/checkout" className="btn btn-flat">
                                            <i className="fa fa-2x fa-shopping-cart text-muted"
                                               style={{"fontSize": "30px"}}></i>
                                        </Link>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <Language isMobile="true" />
                            </li>
                            <MainMenuList isMobile="true"/>

                        </ul>
                    </Menu>
                </nav>
            </div>

        )
    }
}

export default MobileMenu