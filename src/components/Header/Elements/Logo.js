import React from "react";
import {Link} from 'react-router-dom';

import logo from "../../../assets/img/logo.png";

const Logo = (props) => {
    return (
        <div className="col-lg-4 col-8 text-center">
            <div id="logo">
                <Link to="/"><img src={logo} className="img-fluid" alt="MyCircle" title="MyCircle"/></Link>
            </div>
        </div>
    );
}

export default Logo