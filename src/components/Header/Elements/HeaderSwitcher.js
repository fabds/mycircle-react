import React from "react";
import RightMenu from "./RightMenu";
import Language from "./Language";
import MobileMenu from "./MobileMenu";
import Logo from "./Logo";

const HeaderSwitcher = (props) => {
    return (
        <div className="container-fluid">
            <div className="row d-flex d-lg-none row align-items-center">
                <MobileMenu state={props.properties}/>
                <Logo/>
            </div>
            <div className="row align-items-center d-none d-lg-flex justify-content-between">
                {/* Language Block */}
                <Language/>

                {/* Logo Block */}
                <Logo />

                {/* Right Menu Block */}
                <RightMenu state={props.properties} />
            </div>
        </div>
    );
}

export default HeaderSwitcher