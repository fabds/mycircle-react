import React, { Component } from 'react';
import MainMenuList from './Menu/MainMenuList';

class Menu extends Component {

    render()
    {
        return (

            <nav className="header-top">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-12 col-12 d-none d-lg-block text-center">
                            <nav id="nav-menu-container new-menu">
                                <ul className="nav-menu">
                                    <MainMenuList />
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </nav>

        )
    }
}

export default Menu