import React, { Component } from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import Gravatar from 'react-gravatar';
import { FormattedMessage } from "react-intl";
import { userActions } from '../../../services/Users';

class User extends Component {

    constructor(props) {
        super(props);

        this.rightToggle = this.rightToggle.bind(this);
        this.state = {
            rightDropdownOpen: false
        };
    }

    rightToggle() {
        this.setState({
            rightDropdownOpen: !this.state.rightDropdownOpen
        });
    }

    logout() {
        userActions.logout();
        window.location.href = '/';
    }


    render()
    {
        return (

            <ButtonDropdown isOpen={this.state.rightDropdownOpen} toggle={this.rightToggle}>
                <DropdownToggle color="flat" caret>
                    <span className="text-muted" style={{"fontSize": "20px"}}>{this.props.user.firstname}&nbsp;&nbsp;</span>
                    <Gravatar email={(typeof(this.props.user.email) !== "undefined" && this.props.user.email !== '')?this.props.user.email:'dummy@email.com'} size={30} className="rounded-circle" default="mp"/>
                </DropdownToggle>
                <DropdownMenu>
                    <Link className="dropdown-item" to="/account">
                        <FormattedMessage id="account" defaultMessage="Profilo"/>
                    </Link>
                    <Link className="dropdown-item" to="/wishlist">
                        <FormattedMessage id="wishlist" defaultMessage="Wishlist"/>
                    </Link>
                    {/*<Link className="dropdown-item" to="/cart">Carrello</Link>*/}
                    <DropdownItem divider/>
                    <button className="btn dropdown-item" onClick={this.logout}>
                        <FormattedMessage id="logout" defaultMessage="Esci"/>
                    </button>
                </DropdownMenu>
            </ButtonDropdown>

        )
    }
}

const mapState = (state) => ({
    user: state.session.user,
    authenticated: state.session.authenticated
});


export default connect(mapState)(User);