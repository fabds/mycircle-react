import React from "react";
import {Link} from 'react-router-dom';
import {ButtonGroup} from 'reactstrap';

import User from "./User";
import Login from "./Login";
import {connect} from "react-redux";

const RightMenu = (props) => {

    let count = (props.cart_data !== null) ? props.cart_data.items_count : '';

    return (
        <div className="col-lg-4 col-4 d-none d-lg-block text-right">
            <ButtonGroup>
                {props.state.authenticated ? (<User/>) : (<Login/>)}
                <Link to="/wishlist" className="btn btn-flat">
                    <i className="fa fa-2x fa-heart text-muted" style={{"fontSize": "30px"}}></i>
                </Link>
                {<Link to="/checkout" className="btn btn-flat">
                    <i className="fa fa-2x fa-shopping-cart text-muted" style={{"fontSize": "30px"}}></i>
                    <span className="badge badge-pill badge-danger" style={{"float":"right","margin-left":"-14px"}}>{count}</span>
                </Link>}
            </ButtonGroup>
        </div>
    );
}

const mapStateToProps = (state, ownProps) => {
    return {
        cart_data: state.checkout_data.cart_data,
    }
};

export default connect(mapStateToProps)(RightMenu);