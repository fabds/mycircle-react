import React, { Component } from 'react';
import {ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem, ButtonGroup} from 'reactstrap';
import ita_flag from "../../../assets/img/flags/italy.svg";
import eng_flag from "../../../assets/img/flags/eng.png";
import {connect} from "react-redux";
import { setLocale } from "../../../store/actions/Locale";
import {userActions} from "../../../services/Users";


class Language extends Component {

    constructor(props) {
        super(props);

        this.leftToggle = this.leftToggle.bind(this);

        this.state = {
            leftDropdownOpen: false
        };

    }

    leftToggle() {

        this.setState({
            leftDropdownOpen: !this.state.leftDropdownOpen
        });
    }

    changeLanguage() {
        userActions.logout();
        window.location.href = '/';
    }

    languageSwitcher() {
        return (
            <div className="col-lg-4 d-none d-lg-block text-left">
                {/*<div className="btn-group lang-holder" role="group" aria-label="Basic example">*/}
                {/*    <button className={(this.props.lang === 'it')?"btn btn-flat lang-selected":"btn btn-flat"} onClick={() => this.props.setLocale('it')}>*/}
                {/*        <img width="25" className="rounded-circle" src={ita_flag} alt=""/>*/}
                {/*    </button>*/}
                {/*    <button className={(this.props.lang === 'en')?"btn btn-flat lang-selected":"btn btn-flat"} onClick={() => this.props.setLocale('en')}>*/}
                {/*        <img width="25" className="rounded-circle" src={eng_flag} alt=""/>*/}
                {/*    </button>*/}
                {/*</div>*/}

                <ButtonGroup>
                    <ButtonDropdown isOpen={this.state.leftDropdownOpen} toggle={this.leftToggle}>
                        <DropdownToggle color="flat" caret>
                            <span className="text-muted"
                                  style={{"fontSize": "20px"}}>&nbsp;&nbsp;{(this.props.lang === 'it') ? 'it' : 'en'}</span>
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem onClick={this.changeLanguage} onClick={() => window.location.href = '/'+((this.props.lang !== 'it') ? 'it' : 'en')+"/"}>
                                <span className="text-muted"
                                      style={{"fontSize": "20px"}}>&nbsp;&nbsp;{(this.props.lang !== 'it') ? 'it' : 'en'}</span></DropdownItem>

                        </DropdownMenu>
                    </ButtonDropdown>
                </ButtonGroup>

            </div>
        )
    }

    mobileLanguageSwitcher() {
        return (
            <div className="row">
                <div className="col-6 text-center">
                    <button className={(this.props.lang === 'it')?"btn btn-flat lang-selected":"btn btn-flat"} onClick={() => this.props.setLocale('it')}>
                        <img width="25" className="rounded-circle" src={ita_flag} alt=""/>
                    </button>
                </div>
                <div className="col-6 text-center">
                    <button className={(this.props.lang === 'en')?"btn btn-flat lang-selected":"btn btn-flat"} onClick={() => this.props.setLocale('en')}>
                        <img width="25" className="rounded-circle" src={eng_flag} alt=""/>
                    </button>
                </div>
            </div>
        );
    }

    render()
    {
        return this.props.isMobile==="true"?this.mobileLanguageSwitcher():this.languageSwitcher();
    }
}

const mapState = (state) => ({
    lang: state.locale.lang
});

export default connect(mapState, { setLocale })(Language)