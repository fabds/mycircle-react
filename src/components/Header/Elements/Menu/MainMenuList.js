import React, { PureComponent } from 'react';
import {Link} from "react-router-dom";
import { Query } from 'react-apollo';
import { CATEGORIES } from '../../../../graphql/categories';
import { connect } from 'react-redux';
import { setCategories } from "../../../../store/actions/Categories";
import { FormattedMessage } from "react-intl";
import Aux from "../../../../hoc/Aux";

class MainMenuList extends PureComponent {

    submenuToggle() {
        return (
            <Aux>
                <button className="btn btn-flat float-right mobile-menu-toggle">
                    <i className="fa fa-plus-circle"></i>
                </button>
                <div className="clearfix"></div>
            </Aux>
        );
    }

    render()
    {
        return (
            <Aux>
                <Query query={CATEGORIES}
                       variables={{id:3}} >
                    {({ data, error, loading }) => {
                        // //IF ERROR
                        if (error) return "";
                        //IF LOADING
                        if (loading) return (
                            <li>
                                <Link to="/cerchi/esperienze.html">
                                    <FormattedMessage id="nav.experiences" defaultMessage="Esperienze"/>
                                </Link>
                            </li>);
                        //IF SUCCESS
                        const name = data.category.name;
                        const children = data.category.children;
                        //this.setState({ data });
                        //this.props.setCategories(data.category);
                        return (<li><Link className={(this.props.isMobile === "true")?"float-left":""} to="/cerchi/esperienze.html">{name}</Link>
                            {(this.props.isMobile === "true")?this.submenuToggle():""}
                            <ul className={(this.props.isMobile === "true")?"mobile-submenu dis-none":"dropdown-menu main-menu-submenu"}>
                                {children.map(cat => (
                                    <li key={cat.id} ><Link to={"/cerchi/"+cat.url_path+'.html'}>{cat.name}</Link></li>
                                ))}
                            </ul>
                        </li>);
                    }}
                </Query>

                <li><Link to="/how-it-works"><FormattedMessage id="nav.howitworks" defaultMessage="Come Funziona"/></Link></li>
                <li><Link to="/circles"><FormattedMessage id="nav.circles" defaultMessage="Cerchie"/></Link></li>
                <Query query={CATEGORIES}
                       variables={{id:9}} >
                    {({ data, error, loading }) => {
                        // //IF ERROR
                        if (error) return "";
                        //IF LOADING
                        if (loading) return (
                            <li>
                                <Link to="/experiences">
                                    <FormattedMessage id="nav.experiences" defaultMessage="Pacchetti"/>
                                </Link>
                            </li>);
                        //IF SUCCESS
                        const name = data.category.name;
                        const children = data.category.children;
                        //this.setState({ data });
                        //this.props.setCategories(data.category);
                        return (<li><Link className={(this.props.isMobile === "true")?"float-left":""} to="/cerchi/pacchetti.html">{name}</Link>
                            {(this.props.isMobile === "true")?this.submenuToggle():""}
                            <ul className={(this.props.isMobile === "true")?"mobile-submenu dis-none":"dropdown-menu main-menu-submenu"}>
                                {children.map(cat => (
                                    <li key={cat.id} ><Link to={"/cerchi/"+cat.url_path+'.html'}>{cat.name}</Link></li>
                                ))}
                            </ul>
                        </li>);
                    }}
                </Query>
                <li><Link to="/about-us"><FormattedMessage id="nav.aboutus" defaultMessage="Chi Siamo"/></Link></li>
                <li><Link to="/contact"><FormattedMessage id="nav.contacts" defaultMessage="Contatti"/></Link></li>
            </Aux>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        categories: state.categories
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setCategories: categories => dispatch(setCategories(categories)),
    };
}

export default connect(mapStateToProps,mapDispatchToProps)(MainMenuList)