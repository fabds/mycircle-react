import React, {Component} from "react";
import {connect} from 'react-redux';
import {Link} from "react-router-dom";
import Aux from "../../hoc/Aux";
import { FormattedMessage } from "react-intl";

import footerLogo from '../../assets/img/logo-footer.png';

class Footer extends Component {

    /**
     *
     * @returns {*}
     */
    loggedMenu() {
        if(!this.props.authenticated){
            return (
                <Aux>
                    <li><Link to="register"><FormattedMessage id="register" defaultMessage="Registrati"/></Link></li>
                    <li><Link to="login"><FormattedMessage id="login" defaultMessage="Accedi"/></Link></li>
                </Aux>
            )
        }
        else {
            return (
                <li><Link to="account"><FormattedMessage id="account" defaultMessage="Profilo"/></Link></li>
            )
        }
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return (
            <footer className="footer-area">
                <div className="container">

                    <div className="row">
                        <div className="col-lg-4  col-md-6 col-sm-6">
                            <div className="single-footer-widget">
                                <h6>Newsletter</h6>
                                <p>
                                    <FormattedMessage id="subscribe_to_newsletter" defaultMessage="Iscriviti alla nostra newsletter"/>
                                </p>
                                <form>
                                    <input name="email" type="email" className="form-control rounded-0"/>
                                    <br/>
                                    <button type="submit" className="btn btn-primary rounded-0 btn-lg "><FormattedMessage id="subscribe" defaultMessage="Iscriviti"/></button>
                                </form>
                            </div>
                        </div>
                        <div className="col-lg-3 col-md-6 col-sm-6">
                            <div className="single-footer-widget">
                                <h6>About Us</h6>
                                <div className="row">
                                    <div className="col">
                                        <ul>
                                            <li><Link to="/chi-siamo">Chi Siamo</Link></li>
                                            <li><Link to="/mission">Mission</Link></li>
                                            <li><Link to="/partners">Partners</Link></li>
                                            <li><Link to="/contact">Contatti</Link></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-3  col-md-6 col-sm-6">
                            <div className="single-footer-widget">
                                <h6>Note Legali</h6>
                                <div className="row">
                                    <ul>
                                        <div className="col">
                                            <li><Link to="/condizioni-generali">Condizioni Generali</Link></li>
                                            <li><Link to="/privacy">Informativa sulla Privacy</Link></li>
                                            <li><Link to="/area-legale">Area Legale</Link></li>
                                        </div>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div className="col-lg-2  col-md-6 col-sm-6">
                            <div className="single-footer-widget">
                                <h6>Account</h6>
                                <div className="row">
                                    <ul>
                                        <div className="col">
                                            {this.loggedMenu()}
                                            <li>
                                                <Link to="/community">
                                                    <FormattedMessage id="nav.community" defaultMessage="Comunità"/>
                                                </Link>
                                            </li>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="clearfix"></div>
                    <br/><br/>

                    <div className="row">
                        <div className="col-sm-12 footer-social text-center">
                            <a href="facebook">
                                <span className="fa-stack fa-lg">
                                    <i className="far fa-circle-thin fa-stack-2x"></i>
                                    <i className="fab fa-facebook-f fa-stack-1x"></i>
                                </span>
                            </a>
                            <a href="instagram">
                                <span className="fa-stack fa-lg">
                                    <i className="far fa-circle-thin fa-stack-2x"></i>
                                    <i className="fab fa-instagram fa-stack-1x"></i>
                                </span>
                            </a>
                            <a href="linkedin">
                                <span className="fa-stack fa-lg">
                                    <i className="far fa-circle-thin fa-stack-2x"></i>
                                    <i className="fab fa-linkedin fa-stack-1x"></i>
                                </span>
                            </a>
                            <a href="twitter">
                                <span className="fa-stack fa-lg">
                                    <i className="far fa-circle-thin fa-stack-2x"></i>
                                    <i className="fab fa-twitter fa-stack-1x"></i>
                                </span>
                            </a>
                        </div>
                    </div>


                    <div className="row footer-bottom d-flex justify-content-between align-items-center">
                        <p className="col-lg-8 col-sm-12 footer-text m-0">
                            &copy;Copyright {(new Date().getFullYear())} - <FormattedMessage id="all_right_reserved" defaultMessage="All rights reserved "/>
                            <a rel="noopener noreferrer" href={process.env.REACT_APP_BASE_URL} target="_blank">
                                <FormattedMessage id="appname" defaultMessage="App Name"/>
                            </a>
                        </p>
                        <div className="col-lg-4 col-sm-12 text-right">
                            <img alt="footer logo" src={footerLogo} style={{"maxHeight": "26px"}}/>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

/**
 * 
 * @param session
 * @returns {{authenticated: boolean}}
 */
const mapState = ({session}) => ({
    authenticated: session.authenticated
});

export default connect(mapState)(Footer);