import React from "react"

import Header from "../Header/Header";
import Footer from "../Footer/Footer";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const Layout = (props) => {
    return (
        <div className={"MyCircle " + ((window.location.pathname === '/')?'':'internalmenu')}>
          <Header />
          <main>
              {props.children}
          </main>
          <Footer />
        </div>
    )
};


export default Layout