import React from "react";
import ContentLoader from "react-content-loader"

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const TagLoader = props => (
    <ContentLoader
        rtl
        height={160}
        width={400}
        speed={2}
        primaryColor="#f3f3f3"
        secondaryColor="#ecebeb"
        {...props}
    >
        <rect x="9.09" y="20.61" rx="0" ry="0" width="108.64" height="21" />
        <rect x="127.09" y="20.61" rx="0" ry="0" width="108.64" height="21" />
        <rect x="247.09" y="20.61" rx="0" ry="0" width="108.64" height="21" />
        <rect x="9.09" y="52.61" rx="0" ry="0" width="108.64" height="21" />
        <rect x="127.09" y="52.61" rx="0" ry="0" width="108.64" height="21" />
    </ContentLoader>
);

export default TagLoader;