import React from "react";
import LoadingScreen from 'react-loading-screen';

/**
 *
 * @returns {*}
 * @constructor
 */
const FullScreenLoader = () => {
    return (
        <LoadingScreen
            loading={true}
            bgColor='rgba(255,255,255,0.8)'
            spinnerColor='#FF4066'
            textColor='#676767'
            logoSrc='/logo.png'
            text='Caricamento in corso'
        >
            .
        </LoadingScreen>
    )
};

export default FullScreenLoader;