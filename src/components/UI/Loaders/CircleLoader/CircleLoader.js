import React from "react";
import ContentLoader from "react-content-loader"

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleLoader = props => (
  <ContentLoader 
    rtl
    height={400}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
    {...props}
  >
    <circle cx="200" cy="200" r="180" />
  </ContentLoader>
);

export default CircleLoader