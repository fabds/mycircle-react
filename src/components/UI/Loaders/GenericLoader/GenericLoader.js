import React from "react";
import ContentLoader from "react-content-loader"

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const GenericLoader = props => (
  <ContentLoader 
    rtl
    height={40}
    width={400}
    speed={2}
    primaryColor="#f3f3f3"
    secondaryColor="#ecebeb"
    {...props}
  >
  </ContentLoader>
)

export default GenericLoader