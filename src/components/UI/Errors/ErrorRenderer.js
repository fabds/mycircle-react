import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const ErrorRenderer = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="alert alert-danger text-center" role="alert">
                        <h1 className="text-danger">
                            <i className="fas fa-exclamation-triangle"></i>
                        </h1>
                        <br/>
                        <h3>
                            {props.error.message}
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ErrorRenderer;