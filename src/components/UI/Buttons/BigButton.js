import React from "react";
import {Link} from 'react-router-dom';

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const BigButton = (props) => {
    return (
        <Link to={props.link} className="btn btn-primary rounded-0 btn-padding btn-lg text-uppercase">
            {props.name}
        </Link>
    );
};

export default BigButton;