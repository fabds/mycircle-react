import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const SectionTitle = (props) => {
    return (
        <div className="container">
            <div className="row">
                <div className="col-sm-12">
                    <div className="title text-left">
                        <h2 className="text-uppercase">{props.title}</h2>
                        <br/><br/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionTitle