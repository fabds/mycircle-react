import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const BlockSeparator = (props) => {
    return (
        <div className="clearfix separator"></div>
    );
};

export default BlockSeparator