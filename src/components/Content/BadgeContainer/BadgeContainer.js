import React, {Component} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class BadgeContainer extends Component {

    /**
     * Badge Container Render Method
     * @returns {*}
     */
    render(){
        let primary = false;
        return (
            <ul className={this.props.class}>
                {this.props.badges.map((badge) => {
                    primary = (parseInt(this.props.circles_stats.category) === parseInt(badge.id));

                    return (
                        <li key={badge.name} className={(!primary)?"badge font-weight-light badge-secondary":"badge font-weight-light badge-primary"}>
                            <Link className="text-white" to={badge.url_path}>{badge.name}</Link>
                        </li>);
                })}
            </ul>
        );
    }
}

/**
 *
 * @param state
 * @returns {{circles_stats: ({date: Date, city: string, category: string} | ExperiencesList.state.circles_stats | {date, city, category} | Listing.state.circles_stats | {date: null, city: null, category: string} | initialState.circles_stats)}}
 */
const mapStateToProps = (state) => {
    return {
        circles_stats: state.circles_stats
    }
};


export default connect(mapStateToProps)(BadgeContainer);