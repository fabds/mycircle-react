import React from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const BannerArea = (props) => (
    <section className="banner-area relative">
        <div className="overlay overlay-bg"/>
        <div className="container">
            <div className="row align-items-end justify-content-between" style={{"marginTop": "50px","paddingBottom": "230px", "height":"860px"}}>
                <div className="col-lg-12 col-md-12 text-center">
                    <h1 className="text-white" style={{"fontWeight": "100", "fontSize" : "50px"}}>
                        <FormattedMessage id="banner_content.slide1.text" defaultMessage="Text 1"/>
                    </h1>
                    <br/><br/>
                    <Link to="/circles" className="btn btn-primary btn-lg rounded-0 text-uppercase btn-padding mr-20">
                        <FormattedMessage id="banner_content.slide1.button1" defaultMessage="Button 1"/>
                    </Link>
                    <Link to="/circles" className="btn btn-primary btn-lg rounded-0 text-uppercase btn-padding">
                        <FormattedMessage id="banner_content.slide1.button2" defaultMessage="Button 2"/>
                    </Link>
                </div>
            </div>
        </div>
    </section>
);

export default BannerArea;