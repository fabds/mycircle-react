import React, {Component} from 'react';
import CircleSimple from "../../Elements/Circle/CircleSimple";
import Slider from "react-slick";

class CircleSlider extends Component {


    /**
     * Component Did Update
     */
    componentDidUpdate() {

        // if (typeof window.jQuery('.slider') !== "undefined") {
        //     window.initCirclesSlider();
        //     window.jQuery('.slider').resize()
        // }

    }

    /**
     *
     * @param circle
     * @param i
     * @param offset
     * @returns {*}
     */
    getCircle(circle,i,offset) {
        if(typeof(circle) !== 'undefined' && circle != null){

            return <CircleSimple
                key={'slider-'+circle.id}
                circle={circle}
            />;
        }
        else {
            return '';
        }

    }

    /**
     *
     * @param offset
     * @returns {*}
     */
    getSlide(offset){
        let el = [];

        offset = offset * 5;
        //el = this.props.circles.map((circle,i) => this.getCircleHtml(circle,i));

        if(typeof(this.props.circles[offset]) !== 'undefined'){
            el.push(this.getCircle(this.props.circles[0+offset],0,offset));
            el.push(this.getCircle(this.props.circles[1+offset],1,offset));
            el.push(this.getCircle(this.props.circles[2+offset],2,offset));
            el.push(this.getCircle(this.props.circles[3+offset],3,offset));
            el.push(this.getCircle(this.props.circles[4+offset],4,offset));
            return <div key={offset} className={'carousel-item '+((offset === 0)?'active':'')}><div className="circle-slide"><div className="row">{el}</div></div></div>;
        }
        else {
            return "";
        }
    }

    /**
     *
     * @returns {*}
     */
    render(){
        const settings = {
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            variableWidth: true,
            dots: false,
            adaptiveHeight: true,
        };
        return (

            <div className="detailSliderHome">
                <div className="row">
                    <div className="col-12">
                        <Slider {...settings} className="regular-circle circle-slide" >
                            {this.props.circles.map((slide) => this.getCircle(slide,0,0))}
                        </Slider>
                    </div>
                </div>
            </div>
           );
    }
}
    

export default CircleSlider;