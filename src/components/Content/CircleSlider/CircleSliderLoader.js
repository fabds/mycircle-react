import React from "react";
import CircleLoader from "../../UI/Loaders/CircleLoader/CircleLoader"

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleSliderLoader = props => (
    <div className="row">
        <div className="col-lg-4 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-4 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-4 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-4 offset-2 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-4 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
    </div>
  );
  
  export default CircleSliderLoader