import React, {Component} from 'react';

import CircleSimple from "../../Elements/Circle/CircleSimple";
import Aux from "../../../hoc/Aux";

class CircleSlider extends Component {

    /**
     *
     * @param circle
     * @returns {*}
     */
    getCircle(circle) {
        return (
            <div className="col-12 col-md-6 col-lg-4 col-xl-3" key={"wrap-"+circle.id}>
                <CircleSimple key={circle.id} circle={circle} type="alt" />
            </div>
        );
    }

    /**
     *
     * @param circle
     * @param i
     * @returns {*}
     */
    getCircleHtml (circle,i) {
        return this.getCircle(circle);
    }

    /**
     *
     * @returns {Array}
     */
    createPages(){
        const pagenum = this.props.item_num / this.props.size;
        const pnum = Math.ceil(pagenum) + 1;
        let i, pagelink;
        const pages = [];
        for(i=1;i<pnum;i++){
            pagelink = (this.props.page === i)?"":"page-link";
            pages.push(<li key={i} className="page-item">
                <button className={"btn btn-primary "+pagelink} onClick={this.props.act.bind(null,i)}>
                    {i}
                </button>
            </li>);
        }
        return pages;
    }

    /**
     *
     * @returns {*}
     */
    createPagination(){
        return (
            <ul className="pagination">
            {this.createPages()}
            </ul>
        );
    }

    /**
     *
     * @returns {*}
     */
    createInfinityScroll(){
        const pagenum = this.props.item_num / this.props.size;
        const pnum = Math.ceil(pagenum) + 1;
        const nextPage = this.props.page + 1;
        if(nextPage>=pnum){
            return "";
        }
        return (
            <div className="page-trigger">
                <button onClick={this.props.act.bind(null,nextPage)} className="btn btn-primary">Next</button>
            </div>
        );
    }

    /**
     *
     * @returns {*}
     */
    render(){
        return (
            <Aux>
                <div className="container-fluid">
                    <div className="row">
                        {this.props.circles.map((circle,i) => this.getCircleHtml(circle,i))}
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            {(process.env.REACT_APP_PAGINATION_TYPE==="none")?"":(process.env.REACT_APP_PAGINATION_TYPE==="infinityscroll")?this.createInfinityScroll():this.createPagination()}
                        </div>
                    </div>
                </div>
            </Aux>
        );
    }
}


export default CircleSlider;