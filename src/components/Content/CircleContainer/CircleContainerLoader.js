import React from "react";
import CircleLoader from "../../UI/Loaders/CircleLoader/CircleLoader"

/**
 * 
 * @param props
 * @returns {*}
 * @constructor
 */
const CircleContainerLoader = props => (
    <div className="row">
        <div className="col-lg-3 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-3 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-3 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
        <div className="col-lg-3 col-md-6 col-sm-6 text-center">
            <CircleLoader />
        </div>
    </div>
  );
  
  export default CircleContainerLoader