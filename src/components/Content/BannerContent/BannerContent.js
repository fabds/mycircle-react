import React from "react";

import imgdemo from "../../../assets/img/d1.jpg";
import {Link} from "react-router-dom";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const BannerContent = (props) => {
    return (
        <div className="row">
            <div className="col-lg-4">
                <div className="single-destination relative">
                    <div className="thumb relative">
                        <div className="overlay overlay-bg"></div>
                        <img className="img-fluid"
                             src="http://vps123960.ovh.net/mycircle/media/wysiwyg/structures/str_1.jpg" alt="" />
                    </div>
                    <div className="desc">
                        <Link to="/page/1" className="price-btn text-white">Leggi tutto</Link>
                        <h4>Mountain River</h4>
                        <p>Paraguay</p>
                    </div>
                </div>

                <br />

                    <div className="single-destination relative">
                        <div className="thumb relative">
                            <div className="overlay overlay-bg"></div>
                            <img className="img-fluid" src={imgdemo} alt="" />
                        </div>
                        <div className="desc">
                            <Link to="/page/2" className="price-btn text-white">Leggi tutto</Link>
                            <h4>Mountain River</h4>
                            <p>Paraguay</p>
                        </div>
                    </div>
            </div>
            <div className="col-lg-4">
                <div className="single-destination relative">
                    <div className="thumb relative">
                        <div className="overlay overlay-bg"></div>
                        <img className="img-fluid" src={imgdemo} alt="" />
                    </div>
                    <div className="desc">
                        <Link to="/page/3" className="price-btn text-white">Leggi tutto</Link>
                        <h4>Mountain River</h4>
                        <p>Paraguay</p>
                    </div>
                </div>

                <br />

                    <div className="single-destination relative">
                        <div className="thumb relative">
                            <div className="overlay overlay-bg"></div>
                            <img className="img-fluid"
                                 src="http://vps123960.ovh.net/mycircle/media/wysiwyg/structures/str_1.jpg" alt="" />
                        </div>
                        <div className="desc">
                            <Link to="/page/4" className="price-btn text-white">Leggi tutto</Link>
                            <h4>Mountain River</h4>
                            <p>Paraguay</p>
                        </div>
                    </div>
            </div>
            <div className="col-lg-4">
                <div className="single-destination relative">
                    <div className="thumb relative">
                        <div className="overlay overlay-bg"></div>
                        <img className="img-fluid"
                             src="http://vps123960.ovh.net/mycircle/media/wysiwyg/structures/str_1.jpg" alt="" />
                    </div>
                    <div className="desc">
                        <Link to="/page/5" className="price-btn text-white">Leggi tutto</Link>
                        <h4>Mountain River</h4>
                        <p>Paraguay</p>
                    </div>
                </div>

                <br/>

                <div className="single-destination relative">
                    <div className="thumb relative">
                        <div className="overlay overlay-bg"></div>
                        <img className="img-fluid" src={imgdemo} alt="" />
                    </div>
                    <div className="desc">
                        <Link to="/page/6" className="price-btn text-white">Leggi tutto</Link>
                        <h4>Mountain River</h4>
                        <p>Paraguay</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BannerContent