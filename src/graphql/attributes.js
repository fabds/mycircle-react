import gql from 'graphql-tag';

const CITY = gql`    
    {
        customAttributeMetadata(
            attributes: {
                attribute_code: "city"
                entity_type: "4"
            }
        ) {
            items {
                attribute_code
                entity_type
                attribute_type
                attribute_options {
                    value
                    label
                }
            }
        }
    }`;

    export {
        CITY
    }