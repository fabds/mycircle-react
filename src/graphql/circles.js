import gql from 'graphql-tag';

const CIRCLES = gql`
    query Circles($date: String,$recurrent: String,$city: String,$category: String, $page: Int, $size: Int)
    {
        products(
            circles_global_date: $date
            recurrent : $recurrent
            filter: {
                city: {like: $city}
                category_id: {eq: $category}

            }
           
            pageSize: $size,
            currentPage: $page,
            sort: {
                circle_id: DESC
                circle_date: DESC
                circle_end_date: ASC
            }
        )
        {
            sort_fields
            {
                default
                options {
                    value
                    label
                }
            }
            filters
            {
                name
                request_var
                filter_items {
                    label
                    value_string
                    items_count
                }
                filter_items_count
            }
            
            items
            {
                id
                recurrent
                categories{
                    id
                }
                circles {
                    circles_count
                    circles_data {
                        circle_id
                        circle_name
                        current_partecipants
                        guest_partecipants
                        circle_status
                        circle_state
                        promoter_id
                        date
                        open_date
                        end_date
                        group_max_number
                    }

                }
                name
                city
                sku
                date_start
                date_end
                max_partecipants
                image {
                    label
                    url
                }
                thumbnail {
                    label
                    url
                }
                small_image {
                    label
                    url
                }

                price {
                    regularPrice {
                        amount {
                            value
                            currency
                        }
                    }
                    minimalPrice {
                        amount {
                            value
                            currency
                        }
                    }
                }
            }
            filters {
                name
                filter_items_count
                request_var
                filter_items {
                    label
                    value_string
                    items_count
                }
            }
            total_count
            page_info {
                page_size
            }
        }
    }`;

const CIRCLE_DETAIL = gql`

    fragment Configurable on ConfigurableProduct {

        configurable_options {
            id
            attribute_id
            label
            position
            use_default
            attribute_code
            values {
                value_index
                label
            }
            product_id
        }
        variants {
            product {
                id
                name
                sku
                attribute_set_id
                price {
                    regularPrice {
                        amount {
                            value
                            currency
                        }
                    }
                }
            }
            attributes {
                label
                code
                value_index
            }
        }
    }
    
    query CirclesDetails($sku: String)
    {
        products(
            filter: {
                sku: {eq: $sku}

            }
            pageSize: 1,
            currentPage: 1,
            sort: {}
        )
        {
            sort_fields
            {
                default
                options {
                    value
                    label
                }
            }
            items
            {
                id
                type_id
                ...Configurable
                recurrent
                categories{
                    id
                }
                name
                city
                sku
                description {
                    html
                }
                short_description {
                    html
                }
                date_start
                date_end
                max_partecipants
                image {
                    label
                    url
                }
                thumbnail {
                    label
                    url
                }
                small_image {
                    label
                    url
                }
                media_gallery_entries {
                    id
                    media_type
                    label
                    position
                    disabled
                    types
                    file
                    content {
                      base64_encoded_data
                      type
                      name
                    }
                  }
                circles {
                    circles_count
                    circles_data {
                        circle_id
                        circle_name
                        current_partecipants
                        guest_partecipants
                        circle_status
                        circle_state
                        promoter_id
                        date
                        open_date
                        end_date
                        group_max_number
                        partecipants {
                            users_count
                            users_data {
                                customer_id
                                nickname
                                image

                            }
                        }
                    }

                }
                
                price {
                    regularPrice {
                        amount {
                            value
                            currency
                        }
                    }
                    minimalPrice {
                        amount {
                            value
                            currency
                        }
                    }
                }
            }
            
            total_count
            page_info {
                page_size
            }
        }
    }`;


    export {
        CIRCLES,
        CIRCLE_DETAIL
    };