import gql from 'graphql-tag';

const CATEGORIES = gql`
    query CategoryTree($id: Int!) {
  category(id:$id)
  {
    id,
    name,
    path,
    path_in_store,
    url_path,
    url_key
    children {
      id,
      name,
      path,
      path_in_store,
      url_path,
      url_key
    }    
  }
}`;


export {
    CATEGORIES
}