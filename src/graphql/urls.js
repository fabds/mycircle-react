import gql from 'graphql-tag';

const URLS = gql`
    query UrlsDetails($url: String!)
    {
        urlResolver(
    
            url: $url
    
    )
        {
            canonical_url
            id
            type
        }
    }`;


export {
    URLS
}