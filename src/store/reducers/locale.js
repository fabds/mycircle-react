import { SET_LOCALE } from "../types";

/**
 *
 * @param state
 * @param action
 * @returns {*}
 * @constructor
 */
const Locale = (state = { lang : 'it' }, action = {}) => {

    switch (action.type) {
        case SET_LOCALE:
            return { lang: action.lang };
        default:
            return state;
    }
};

export default Locale