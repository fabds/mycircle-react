import {SET_CART_DATA, SET_PAYMENT_DATA} from "../types";

/**
 *
 * @param state
 * @param action
 * @returns {*}
 * @constructor
 */
const Checkoutdata = (state = { cart_data : null }, action) => {

    switch (action.type) {
        case SET_CART_DATA:
            return  {
                    ...state,
                cart_data: action.cart_data,
                }
        case SET_PAYMENT_DATA:
            return  {
                ...state,
                payment_data: action.payment_data,
            }
        default:
            return state
    }
};

export default Checkoutdata