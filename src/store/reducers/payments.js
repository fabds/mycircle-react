import { SET_PAYMENT_DATA } from "../types";

/**
 *
 * @param state
 * @param action
 * @returns {{payment_data: null}}
 * @constructor
 */
const Paymentdata = (state = { payment_data : null }, action) => {

    switch (action.type) {
        case SET_PAYMENT_DATA:
            return  {
                ...state,
                payment_data: action.payment_data,
            }
        default:
            return state
    }
};

export default Paymentdata