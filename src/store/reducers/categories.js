import { SET_CATEGORIES } from "../types"

/**
 *
 * @param state
 * @param action
 * @returns {{categories: Array}}
 * @constructor
 */
const Categories = (state = { categories : [] }, action) => {

    switch (action.type) {
        case SET_CATEGORIES:
            return  {
                ...state,
                categories: action.categories,
            }
        default:
            return state
    }
};

export default Categories