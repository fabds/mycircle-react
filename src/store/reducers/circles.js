import { SET_CITY, SET_CATEGORY, SET_DATE} from "../types";

/**
 *
 * @param state
 * @param action
 * @returns {{city: null, category: string, date: null}}
 * @constructor
 */
const CircleStats = (state = { city : null, category : '2', date: null }, action) => {

    switch (action.type) {
        case SET_DATE:
            return  {
                    ...state,
                    date: action.date,
                }
        case SET_CITY:
            return  {
                ...state,
                city: action.city,
            }
        case SET_CATEGORY:
            return  {
                ...state,
                category: action.category,
            }
        default:
            return state
    }
};

export default CircleStats