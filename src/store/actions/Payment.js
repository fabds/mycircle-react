import { SET_PAYMENT_DATA } from "../types";

/**
 *
 * @param payment_data
 * @returns {{type: string, payment_data: *}}
 */
export const setPaymentdata = payment_data  => ({
    type: SET_PAYMENT_DATA,
    payment_data
});