import { SET_CART_DATA } from "../types";
import { SET_PAYMENT_DATA } from "../types";

/**
 *
 * @param cart_data
 * @returns {{type: string, cart_data: *}}
 */
export const setCartdata = cart_data  => ({
    type: SET_CART_DATA,
    cart_data
});

/**
 *
 * @param payment_data
 * @returns {{type: string, payment_data: *}}
 */
export const setPaymentdata = payment_data  => ({
    type: SET_PAYMENT_DATA,
    payment_data
});