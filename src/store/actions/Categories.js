import { SET_CATEGORIES } from "../types";

/**
 *
 * @param categories
 * @returns {{type: string, categories: *}}
 */
export const setCategories = categories  => ({
    type: SET_CATEGORIES,
    categories
});