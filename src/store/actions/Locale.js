import { SET_LOCALE } from "../types"

/**
 *
 * @param lang
 * @returns {{type: string, lang: *}}
 */
export const localeSet = lang  => ({
    type: SET_LOCALE,
    lang
});

/**
 *
 * @param lang
 * @returns {Function}
 */
export const setLocale = lang => (dispatch) => {
    localStorage.myCircleLang = lang;
    dispatch(localeSet(lang));
};