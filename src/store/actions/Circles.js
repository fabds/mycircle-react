import { SET_CITY, SET_CATEGORY, SET_DATE} from "../types";

/**
 *
 * @param date
 * @returns {{type: string, date: *}}
 */
export const setDate = date  => ({
    type: SET_DATE,
    date
});

/**
 *
 * @param city
 * @returns {{type: string, city: *}}
 */
export const setCity = city => ({
    type: SET_CITY,
    city
});

/**
 *
 * @param category
 * @returns {{type: string, category: *}}
 */
export const setCategory = category => ({
    type: SET_CATEGORY,
    category
});