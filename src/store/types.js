/**
 *
 * @type {string}
 */
export const SESSION = 'session';

/**
 *
 * @type {string}
 */
export const SET_LOCALE = "SET_LOCALE";

/**
 *
 * @type {string}
 */
export const SET_CATEGORIES = "SET_CATEGORIES";

/**
 *
 * @type {string}
 */
export const SET_DATE = "SET_DATE";

/**
 *
 * @type {string}
 */
export const SET_CITY = "SET_CITY";

/**
 * 
 * @type {string}
 */
export const SET_CATEGORY = "SET_CATEGORY";

/**
 *
 * @type {string}
 */
export const SET_CART_DATA = "SET_CART_DATA";

/**
 * 
 * @type {string}
 */
export const SET_PAYMENT_DATA = "SET_PAYMENT_DATA";
