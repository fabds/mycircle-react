import { URLS } from '../graphql/urls';
import React from "react";
import { Query } from 'react-apollo';

/**
 *
 * @type {{getCountries: (function(): (Q.Promise<any> | * | Promise<T>))}}
 */
export const urlsActions = {
    decodeUrl
};

/**
 *
 * @returns {Q.Promise<any> | * | Promise<T>}
 */
function decodeUrl(url) {

    return (
        <Query query={URLS}
               variables={{url:url}} >
            {({ data, error, loading }) => {
                //IF ERROR
                if (error) console.log(error);
                console.log(data);
                //IF SUCCESS
                return data;
            }}
        </Query>
    )

}
