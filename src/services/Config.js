import api_client from "../providers/api_client";

/**
 *
 * @type {{getCountries: (function(): (Q.Promise<any> | * | Promise<T>))}}
 */
export const configActions = {
    getCountries
};

/**
 *
 * @returns {Q.Promise<any> | * | Promise<T>}
 */
function getCountries() {

    return api_client.get('/directory/countries', null)
        .then(res => {
            return res.data;
        })
        .catch(res => {
            return Promise.reject(res.data.message);
        })

}

/**
 *
 * @param country
 * @returns {Q.Promise<any> | * | Promise<T>}
 */
// function getProvince(country) {
//
//     return api_client.get('/directory/countries/'+country, null)
//         .then(res => {
//             return res.data;
//         })
//         .catch(res => {
//             return Promise.reject(res.data.message);
//         })
//
// }
