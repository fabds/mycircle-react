import api_client from "../providers/api_client";
import {sessionService} from "redux-react-session";

/**
 *
 * @type {{login: (function(*, *): (Q.Promise<any> | * | Promise<T>)), logout: (function(): {type: string}), register: register, getUserdata: (function(): *)}}
 */
export const userActions = {
    login,
    logout,
    register,
    getUserdata,
    getCartdata,
    getTotals,
    getPaymentMethods,
    addCart,
    emptyCart
};

/**
 *
 */
// function getUserSession() {
//
//    console.log(sessionStorage.getItem('token'));
//
// }

/**
 *
 * @returns {*}
 */
function getSessionToken() {

    return sessionService.loadSession().then( session => { return session.token });

}

/**
 *
 * @param nickname
 * @param firstname
 * @param lastname
 * @param username
 * @param password
 * @param confirm_password
 * @returns {*}
 */
function register(nickname, firstname, lastname, username, password, confirm_password) {

    if (password !== confirm_password)
        return null;

    var user = {
        "customer": {
            "firstname" : firstname,
            "lastname"  : lastname,
            "email" : username,
            "storeId": 1,
            "websiteId": 1,
            "custom_attributes": [
                {
                    "attribute_code": "nickname",
                    "value": nickname
                }
            ]
        },
        "password" : password
    }

    return api_client.post('/customers', JSON.stringify(user))
        .then(res => {
            return res.data;
        })
        .catch(res => {
            return Promise.reject(res);
        })
        .then(res => {
            return this.login(res.email, password);
        })
        .then(token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return sessionService.saveSession({token})

        })
        .then(token => {
            return this.getUserdata();
        })
        .then(user_data => {
            sessionService.saveUser(user_data)
        })
        .then(user_data => {
                window.location.href = "/";
            }
        )
        .catch(res => {
            return Promise.reject(res.response.data.message);
        })

}

/**
 *
 * @param username
 * @param password
 * @returns {Q.Promise<any> | * | Promise<T>}
 */
function login(username, password) {

    return api_client.post('/integration/customer/token', JSON.stringify({ username, password }))
        .then(res => {
            // console.log(res.data);
            return res.data;
        })
        .then(token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return sessionService.saveSession({"token":token});


        })
        .then (token => {
            return this.getUserdata();
        })
        .then(user_data => {
            sessionService.saveUser(user_data).then(
                window.location.href = "/"
            );
        })
        .catch(res => {
            return Promise.reject(res.data.message);
        })

}

/**
 *
 * @returns {{type: string}}
 */
function logout() {
    let del_sess = sessionService.deleteSession();
    let del_user = sessionService.deleteUser();

    Promise.all([del_user,del_sess]).then();

    return { type: "logout" };
}

/**
 *
 * @returns {*}
 */
function getUserdata() {

    return getSessionToken().then(
        token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return api_client.get('/customers/me')
                .then(res => {
                    return res.data;
                })
                .catch(res => {
                    return null;
                });

        }
    );

}

/**
 *
 * @returns {*}
 */
function getCartdata() {

    return getSessionToken().then(
        token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return api_client.get('/carts/mine')
                .then(res => {
                    return res.data;
                })
                .catch(res => {
                    return null;
                });

        }
    );



}

function getTotals() {

    return getSessionToken().then(
        token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return api_client.get('carts/mine/totals')
                .then(res => {
                    return res.data;
                })
                .catch(res => {
                    return null;
                });

        }
    );



}

/**
 *
 * @returns {*}
 */
function getPaymentMethods() {

    return getSessionToken().then(
        token => {

            api_client.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            return api_client.get('carts/mine/payment-methods')
                .then(res => {
                    return res.data;
                })
                .catch(res => {
                    return null;
                });

        }
    );

}

function addCart(product_sku, qty, cart_data) {

    if (product_sku <= 0)
        return null;

    let cart = {
        "cartItem": {
            "sku": product_sku,
            "qty": qty,
            "quote_id": cart_data.id
        }
    }

    let items_to_delete = this.emptyCart(cart_data);
    // let item_to_add = api_client.post('/carts/mine/items', JSON.stringify(cart));

    // items_to_delete.push(item_to_add);

    return Promise.all(items_to_delete)
        .then(() => {
            return  api_client.post('/carts/mine/items', JSON.stringify(cart));
        })
        .then(() => {
            return this.getCartdata();
        })
        .then((cart_data) => {
            return cart_data;
        })
        .catch(res => {
            return Promise.reject(res);
        })

}

function emptyCart(cart_data) {

    let items = cart_data.items;
    let promises = [];

    items.forEach(function(element) {

        let item = { "itemId": element.item_id }

        promises.push(
            api_client.delete('/carts/mine/items/'+element.item_id, JSON.stringify(item))
        );
    });

    return promises;

}