import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const NotFound = (props) => {
    return (
        <section className="popular-destination-area section-gap">
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="container">
                <div className="row">
                    <div className="col-md-12 text-center">
                        <h1 className="text-center">404 NotFound</h1>
                        <br/><br/>
                        <img style={{"border": "5px dashed #666"}} src="https://media.giphy.com/media/9J7tdYltWyXIY/giphy.gif" alt="404" />
                    </div>
                </div>
            </div>
        </section>
    )
};

export default NotFound