import React, { PureComponent } from "react";


import CircleSlider from "../../components/Content/CircleSlider/CircleSlider";
import CircleSliderLoader from "../../components/Content/CircleSlider/CircleSliderLoader";
import CircleSearch from "../../components/Elements/Forms/CircleSearch";
import CircleContainer from "../../components/Content/CircleContainer/CircleContainer";
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CIRCLES } from '../../graphql/circles';
import SectionTitle from "../../components/UI/Titles/SectionTitles";
import BlockSeparator from "../../components/UI/Separators/BlockSeparator";
import BannerContent from "../../components/Content/BannerContent/BannerContent";
import SectionContainer from "../../hoc/SectionContainer";
import BadgeContainer from "../../components/Content/BadgeContainer/BadgeContainer";
import {CATEGORIES} from "../../graphql/categories";
import ErrorRenderer from "../../components/UI/Errors/ErrorRenderer";


class Listing extends PureComponent {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            circles: [],
            loading: false,
            badges : [],
            circles_stats: {
                date: new Date(),
                city: '%',
                category: '2'
            }
        };
    }

    /**
     *
     * @param $
     */
    initCircleCounter($) {
        $('body .countdown').each(function (i, v) {
            $(this).circleTimer({ debug: false });
        });
    }

    /**
     *
     * @param $
     */
    initCircleStars($) {
        $('.stars-elements').empty();
        $('.stars-elements').each(function (i, v) {
            $(this).stars();
        });
    }

    /**
     *
     */
    componentDidUpdate() {
        this.initCircleStars(window.jQuery);
        this.initCircleCounter(window.jQuery);
        window.adjustHeader();
    }

    /**
     *
     * @returns {*}
     */
    render() {
        var date = this.props.circles_stats.date ? this.props.circles_stats.date : this.state.circles_stats.date;
        var city = this.props.circles_stats.city ? this.props.circles_stats.city : this.state.circles_stats.city;
        var category = this.props.circles_stats.category ? this.props.circles_stats.category : this.state.circles_stats.category;
        return (
            <div>

                <section className="popular-destination-area section-gap">
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />
                    <CircleSearch />

                    <BlockSeparator />

                    <Query query={CIRCLES} variables={{ date,city, category }}>
                        {({ data, error, loading }) => {
                            //IF ERROR
                            if (error) return <ErrorRenderer error={error}/>;
                            //IF LOADING
                            if (loading) return <CircleSliderLoader />;
                            //IF SUCCESS
                            const circles = data.products.items;
                            this.setState({ circles });
                            return <CircleSlider circles={this.state.circles} />;
                        }}
                    </Query>


                    <BlockSeparator />

                    <SectionTitle title="Sconti Sicuri"/>

                    <SectionContainer class="text-justify">

                        <Query query={CATEGORIES}>
                            {({ data, error, loading }) => {
                                //IF ERROR
                                if (error) return <ErrorRenderer error={error}/>;
                                //IF LOADING
                                if (loading) return <CircleSliderLoader />;
                                //IF SUCCESS
                                const badges = data.category.children;

                                this.setState({ badges });
                                return <BadgeContainer class="categories" badges={this.state.badges} />;
                            }}
                        </Query>

                    </SectionContainer>

                    <BlockSeparator />

                    <CircleContainer circles={this.state.circles} />

                    <BlockSeparator />

                    <SectionTitle title="Potrebbe Interessarti"/>

                    <BannerContent />

                </section>
            </div>
        );
    }
}

/**
 *
 * @param state
 * @returns {{circles_stats: ({date: Date, city: string, category: string}|Listing.state.circles_stats|{date, city, category}|ExperiencesList.state.circles_stats|{date: null, city: null, category: string}|initialState.circles_stats)}}
 */
const mapStateToProps = (state) => {
    return {
        circles_stats: state.circles_stats
    }
}

export default connect(mapStateToProps)(Listing);
