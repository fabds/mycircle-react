import React from "react"

import {Link} from "react-router-dom";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const Cart = (props) => {
    return (
        <section className="popular-destination-area section-gap">
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <h1 className="text-center">Carrello</h1>
                        <br/><br/>
                        <table id="cart" className="table table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th style={{"width": "50%"}}>Cerchia</th>
                                    <th style={{"width": "10%"}}>Prezzo</th>
                                    <th style={{"width": "8%"}}>Quantità</th>
                                    <th style={{"width": "22%"}} className="text-center">Subtotale</th>
                                    <th style={{"width": "10%"}}></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td data-th="Product">
                                        <div className="row">
                                            <div className="col-sm-3 hidden-xs text-center">
                                                <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                            </div>
                                            <div className="col-sm-9">
                                                <h4 className="nomargin">Circle 1</h4>
                                                <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">€1.99</td>
                                    <td data-th="Quantity">
                                        <input type="number" className="form-control text-center" value="1"/>
                                    </td>
                                    <td data-th="Subtotal" className="text-center">€ 1.99</td>
                                    <td className="actions text-center" data-th="">
                                        <button className="btn btn-info btn-sm"><i className="fa fa-sync"></i></button>
                                        &nbsp;&nbsp;
                                        <button className="btn btn-danger btn-sm"><i className="fa fa-trash"></i></button>
                                    </td>
                                </tr>

                                <tr>
                                    <td data-th="Product">
                                        <div className="row">
                                            <div className="col-sm-3 hidden-xs text-center">
                                                <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                            </div>
                                            <div className="col-sm-9">
                                                <h4 className="nomargin">Circle 2</h4>
                                                <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">€ 1.99</td>
                                    <td data-th="Quantity">
                                        <input type="number" className="form-control text-center" value="1"/>
                                    </td>
                                    <td data-th="Subtotal" className="text-center">€ 1.99</td>
                                    <td className="actions text-center" data-th="">
                                        <button className="btn btn-info btn-sm"><i className="fa fa-sync"></i></button>
                                        &nbsp;&nbsp;
                                        <button className="btn btn-danger btn-sm"><i className="fa fa-trash"></i></button>
                                    </td>
                                </tr>

                                <tr>
                                    <td data-th="Product">
                                        <div className="row">
                                            <div className="col-sm-3 hidden-xs text-center">
                                                <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                            </div>
                                            <div className="col-sm-9">
                                                <h4 className="nomargin">Circle 3</h4>
                                                <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                            </div>
                                        </div>
                                    </td>
                                    <td data-th="Price">€ 1.99</td>
                                    <td data-th="Quantity">
                                        <input type="number" className="form-control text-center" value="1"/>
                                    </td>
                                    <td data-th="Subtotal" className="text-center">€ 1.99</td>
                                    <td className="actions text-center" data-th="">
                                        <button className="btn btn-info btn-sm"><i className="fa fa-sync"></i></button>
                                        &nbsp;&nbsp;
                                        <button className="btn btn-danger btn-sm"><i className="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr className="d-none d-xs">
                                    <td className="text-center"><strong>Totale € 1.99</strong></td>
                                </tr>
                                <tr>
                                    <td><Link to="/" className="btn btn-primary">
                                        <i className="fas fa-angle-left"></i> Continua lo Shopping</Link>
                                    </td>
                                    <td colSpan="2" className="hidden-xs"></td>
                                    <td className="hidden-xs text-center">
                                        <strong>Totale € 1.99</strong>
                                    </td>
                                    <td>
                                        <Link to="/checkout" className="btn btn-primary btn-block">
                                            Checkout <i className="fas fa-angle-right"></i>
                                        </Link>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Cart