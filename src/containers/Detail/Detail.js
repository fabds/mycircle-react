import React, { PureComponent } from "react"

//import DetailLoader from "../../components/UI/Loaders/DetailLoader/DetailLoader"
import { Query } from 'react-apollo';
import { CIRCLE_DETAIL } from '../../graphql/circles';
import CircleSimple from "../../components/Elements/Circle/CircleSimple";
import DetailSlider from "./Elements/DetailSlider";
import BookingForm from "./Elements/BookingForm";
import MainContent from "./Elements/MainContent";
import SectionTitle from "../../components/UI/Titles/SectionTitles";
import BannerContent from "../../components/Content/BannerContent/BannerContent";
import BlockSeparator from "../../components/UI/Separators/BlockSeparator";
import ErrorRenderer from "../../components/UI/Errors/ErrorRenderer";
import FullScreenLoader from "../../components/UI/Loaders/FullScreenLoader/FullScreenLoader";
import ExperiencesList from "../Experiences/ExperiencesList";
import {Redirect} from "react-router-dom";


class Detail extends PureComponent {

    /**
     * Component Did Update
     */
    componentDidUpdate() {
        this.initCircleStars(window.jQuery);
        this.initCircleCounter(window.jQuery);
        window.initDetailPage();
        window.jQuery('.slider').resize()
    }

    /**
     *
     * @param $
     */
    initCircleCounter($) {
        $('body .countdown').circleTimerStop();
        $('body .countdown-counter-item').html('-');
        $('body .countdown').each(function (i, v) {
            $(this).circleTimer({ debug: false });
        });
    }

    /**
     *
     * @param $
     */
    initCircleStars($) {
        $('.stars-elements').empty();
        $('.stars-elements').each(function (i, v) {
            $(this).stars();
        });
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return (
            <section className="popular-destination-area responsive-gap">
                <div className="clearfix separator"></div>
                <div className="clearfix separator"></div>
                <div className="clearfix separator"></div>

                <Query query={CIRCLE_DETAIL} variables={{ 'sku': this.props.match.params.slug }}>
                    {({ data, error, loading }) => {
                        //IF ERROR
                        if (error) return <ErrorRenderer error={error}/>;
                        //IF LOADING
                        if (loading) return <FullScreenLoader />;
                        //IF SUCCESS

                        const circle = data.products.items[0];
                        this.setState({ circle: circle });

                        if (typeof data.products.items[0] !== "undefined")
                            return (
                                <div>
                                    <DetailSlider circle={circle}/>

                                    <div className="container-counter-detail text-center">
                                        <CircleSimple
                                            circle={circle}
                                            type="mini"
                                        />
                                    </div>

                                    <div className="clearfix"></div><br/><br/>

                                    <div className="container-fluid middle-page">
                                        <div className="row">
                                            <div className="col-lg-8 col-xl-8 col-12">
                                                <MainContent circle={circle}/>
                                            </div>
                                            <div className="col-lg-4 col-xl-4 col-12">
                                                <BookingForm circle={circle}/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            )
                        else
                            return <Redirect to='/404' />;


                    }}
                </Query>

                <BlockSeparator />

                <SectionTitle title="Potrebbe Interessarti"/>

                <BannerContent />

            </section>
        )
    }
}

export default Detail