import React, { PureComponent } from "react";
import Moment from "react-moment";
import Simple from "./Cart/Simple";
import Configurable from "./Cart/Configurable";


/**
 *
 * @param this.props
 * @returns {*}
 * @constructor
 */

class BookingForm extends PureComponent {

    /**
     *
     * @param circle
     * @param i
     * @param offset
     * @returns {*}
     */
    getCartForm(product) {

        if(typeof(product) !== 'undefined' && product.type_id === "configurable"){

            return <Configurable
                key={'productform-'+product.id}
                circle={product}
            />;
        }
        else {
            return <Simple
                key={'productform-'+product.id}
                circle={product}
            />;
        }

    }

    render() {

        return (
            <div >
                {this.getCartForm(this.props.circle)}
            </div>
        )
    }
};

export default BookingForm