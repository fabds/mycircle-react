import React from "react";

import Aux from "../../../hoc/Aux";
import PartecipantSlider from "./PartecipantSlider";
import DetailsMainImage from "./DetailsMainImage";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const MainContent = (props) => {
    return (
        <Aux>

            <p className="text-justify" dangerouslySetInnerHTML={{__html: props.circle.description.html}}></p>
            <p id="readmore" className="collapse text-justify"  dangerouslySetInnerHTML={{__html: props.circle.short_description.html}}></p>
            <p>
                <a className="collapse-toggle" data-toggle="collapse" href="#readmore" role="button" aria-expanded="false" aria-controls="readmore">
                    Leggi tutto
                </a>
            </p>
            <br/><br/>
            <DetailsMainImage circle={props.circle} />
            <br/><br/>
            <PartecipantSlider circle={props.circle.circles.circles_data} />
        </Aux>
    )
};

export default MainContent