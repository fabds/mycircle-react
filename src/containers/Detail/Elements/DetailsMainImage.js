import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const DetailsMainImage = (props) => {

    var gallery = (typeof props.circle !== "undefined") ? props.circle.media_gallery_entries : [];

    return (
        <div className="images">
            <div className="row">
                {gallery.map(function (item) {
                    if (item.disabled === false) {
                        return (
                            <div className="col-4" key={item.id}>
                                <img alt={props.circle.name} style={{'width':'100%'}} src={process.env.REACT_APP_API_URL+process.env.REACT_APP_PRODUCT_IMAGE_PATH+item.file} className="img-fluid" />
                            </div>
                        );
                    }
                    return (<div key={item.id} />);
                })}

            </div>
        </div>
    )
};

export default DetailsMainImage;