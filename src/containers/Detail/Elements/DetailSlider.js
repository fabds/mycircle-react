import React from "react";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */

const DetailSlider = (props) => {

    var gallery = (typeof props.circle !== "undefined") ? props.circle.media_gallery_entries : [];

    return (
        <div className="detailSlider">
            <div className="row">
                <div className="col-12">
                    <section className="regular slider">
                        <div className="paddingl10 mask-height-420 slick-min-image-width">
                            <img src={props.circle.image.url} alt="" className="img-fluid" />
                        </div>
                        {gallery.map(function (item) {
                            if (item.disabled === true && !item.types.includes("image")) {
                                return (
                                    <div className="paddingl10 mask-height-420 slick-min-image-width" key={item.id}>
                                        <img src={process.env.REACT_APP_API_URL+process.env.REACT_APP_PRODUCT_IMAGE_PATH+item.file} alt="" className="img-fluid" />
                                    </div>
                                );
                            }
                            return (<div key={item.id} />);
                        })}

                    </section>
                </div>
            </div>
        </div>
    )
};

export default DetailSlider;