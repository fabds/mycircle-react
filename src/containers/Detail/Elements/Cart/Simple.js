import React, { PureComponent } from "react";
import {userActions} from "../../../../services/Users";
import Moment from "react-moment";
import {connect} from "react-redux";
import {setCartdata} from "../../../../store/actions/Cart";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */

class Simple extends PureComponent {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            product_sku: this.props.circle.sku,
            qty: 1,
            submitted: false,
            cart_id: '',
            cartmsg: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    /**
     *
     * @param e
     */
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    /**
     *
     * @param e
     */
    handleSubmit(e) {
        e.preventDefault();

        const { product_sku, qty} = this.state;

        if (product_sku && qty >= 0) {

            userActions.addCart(product_sku, qty, this.props.cart_data).then(
                cart=> {
                    if (cart === null) {
                        this.setState({cartmsg:'no_cart'})
                    } else {
                        this.setState({'cart_id': this.props.cart_data.id});
                        this.props.setCartdata(cart);
                        this.setState({cartmsg:'ok_cart'});
                    }
                }
            ).catch(
                message => {
                    this.setState({cartmsg:JSON.stringify({message})})
                })

        }

        else {
            this.setState({cartmsg:"no_qty"})
        }

        this.setState({ submitted: true });

    }


    render() {

        const { qty } = this.state;

        return (
            <form id="add-to-cart" onSubmit={this.handleSubmit}>
                <div className="" style={{background: "#EDE0E7", paddingTop: "20px"}}>
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-10">
                                <h4 className="text-uppercase">{this.props.circle.name}</h4>
                                {
                                    (this.props.circle.date_start !== null && this.props.circle.date_end !== null)
                                        ?
                                        <h6>dal <Moment
                                            format="DD/MM/YYYY">{this.props.circle.date_start}</Moment> al <Moment
                                            format="DD/MM/YYYY">{this.props.circle.date_end}</Moment></h6>
                                        :
                                        ""
                                }
                            </div>
                            <div className="col-2">
                                <button type="button" className="btn btn-default bg-transparent border rounded-0">
                                    <i className="fa fa-calendar"></i>
                                </button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <br/>
                                <input type="hidden" name="product_sku" value={this.props.circle.sku} />
                                <label className="text-uppercase" required>Qty</label>
                                <select onChange={this.handleChange} name="qty" value={qty} className="form-control  form-control-lg rounded-0" required>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                                <div className="message">{this.state.cartmsg}</div>
                                <br/>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <button type="submit" className="rounded-0 btn btn-primary btn-lg btn-block text-uppercase">Acquista</button>

            </form>
        )
    }

};

const mapStateToProps = (state, ownProps) => {
    return {
        cart_data: state.checkout_data.cart_data,
    }
};

function mapDispatchToProps(dispatch) {
    return {
        setCartdata: cart_data => dispatch(setCartdata(cart_data)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Simple);