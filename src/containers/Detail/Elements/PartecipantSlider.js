import React from "react";
import Slider from "react-slick";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const PartecipantSlider = (props) => {

    var users = (typeof props.circle[0] !== "undefined") ? props.circle[0].partecipants.users_data : [];

    const settings = {
        infinite: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        centerMode: false,
        variableWidth: true,
        dots: false,
        adaptiveHeight: true,
    };

    return (
        <div className="partecipants">
            <Slider {...settings}  >
                {users.map(function (item) {
                        return (
                            <div key={item.customer_id}>
                                <img className="rounded-circle" alt="" src="http://placehold.it/80x80?text=1" />
                            </div>
                        );
                })}
            </Slider>
        </div>
    );
};

export default PartecipantSlider