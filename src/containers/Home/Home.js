import React, {PureComponent} from "react";

import BannerArea from '../../components/Content/BannerArea/BannerArea';
import CircleSlider from "../../components/Content/CircleSlider/CircleSlider";
import CircleSearch from "../../components/Elements/Forms/CircleSearch";
import CircleContainer from "../../components/Content/CircleContainer/CircleContainer";
import CircleContainerLoader from "../../components/Content/CircleContainer/CircleContainerLoader";
import TagLoader from "../../components/UI/Loaders/TagLoader/TagLoader";
import {connect} from 'react-redux';
import {Query} from 'react-apollo';

import {CIRCLES} from '../../graphql/circles';
import SectionTitle from "../../components/UI/Titles/SectionTitles";
import BlockSeparator from "../../components/UI/Separators/BlockSeparator";
import BannerContent from "../../components/Content/BannerContent/BannerContent";
import SectionContainer from "../../hoc/SectionContainer";
import BadgeContainer from "../../components/Content/BadgeContainer/BadgeContainer";
import {CATEGORIES} from "../../graphql/categories";
import BigButton from "../../components/UI/Buttons/BigButton";
import ErrorRenderer from "../../components/UI/Errors/ErrorRenderer";
import FullScreenLoader from "../../components/UI/Loaders/FullScreenLoader/FullScreenLoader";


class Home extends PureComponent {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            circles: [],
            circles_slider: [],
            loading: false,
            badges: [],
            size: process.env.REACT_APP_ELEMENT_PER_PAGE,
            recurrent: 0,
            page: 1,
            sizeSlider: process.env.REACT_APP_SLIDER_ELEMENTS,

        };
        this.goToPage = this.goToPage.bind(this)
    }

    /**
     *
     * @param $
     */
    initCircleCounter($) {
        $('body .countdown').each(function (i, v) {
            $(this).circleTimer({debug: false});
        });
    }

    /**
     *
     * @param $
     */
    initCircleStars($) {
        $('.stars-elements').empty();
        $('.stars-elements').each(function (i, v) {
            $(this).stars();
        });
    }

    /**
     *
     * @param page
     */
    goToPage(page) {
        this.setState({page: page});
    }

    /**
     *
     */
    componentDidUpdate() {
        this.initCircleStars(window.jQuery);
        this.initCircleCounter(window.jQuery);
        window.adjustHeader();
    }

    /**
     *
     */
    componentDidMount() {
        this.initCircleStars(window.jQuery);
        this.initCircleCounter(window.jQuery);
        window.adjustHeader();
    }

    /**
     *
     * @returns {*}
     */
    render() {
        var variables = {};

        if (this.props.circles_stats.date) variables.date = new Date(this.props.circles_stats.date).toISOString().slice(0, 10);
        if (this.props.circles_stats.city) variables.city = this.props.circles_stats.city;
        if (this.props.circles_stats.category) variables.category = this.props.circles_stats.category;
        if (this.props.circles_stats.recurrent) variables.recurrent = this.props.circles_stats.recurrent;
        variables.size = this.state.sizeSlider;
        variables.page = this.state.page;
        var page = this.state.page;
        var size = 8;

        return (
            <div>
                <BannerArea/>
                <section className="popular-destination-area section-gap">

                    <CircleSearch/>

                    <BlockSeparator/>

                    <Query query={CIRCLES}
                           variables={variables}>
                        {({data, error, loading}) => {
                            //IF ERROR
                            if (error) return <ErrorRenderer error={error}/>;
                            //IF LOADING
                            if (loading) return <FullScreenLoader/>;
                            //IF SUCCESS
                            const circles_slider = data.products.items;
                            this.setState({circles_slider});
                            return <CircleSlider circles={circles_slider}/>;
                        }}
                    </Query>

                    <BlockSeparator/>

                    <SectionContainer class="text-center">
                        <BigButton name="Mostra Tutti" link="/listing"/>
                    </SectionContainer>

                    <BlockSeparator/>

                    <SectionTitle title="Contenuti in evidenza"/>

                    <BannerContent/>

                    <BlockSeparator/>

                    <SectionTitle title="Sconti Sicuri"/>

                    <SectionContainer class="text-justify">

                        <Query query={CATEGORIES}
                               variables={{id:3}}>
                            {({data, error, loading}) => {
                                //IF ERROR
                                if (error) return <ErrorRenderer error={error}/>;
                                //IF LOADING
                                if (loading) return <TagLoader/>;
                                //IF SUCCESS
                                const badges = data.category.children;

                                this.setState({badges});
                                return <BadgeContainer class="categories" badges={this.state.badges}/>;
                            }}
                        </Query>

                    </SectionContainer>

                    <BlockSeparator/>

                    <Query query={CIRCLES} variables={{page, size}}>
                        {({data, error, loading}) => {
                            //IF ERROR
                            if (error) return <ErrorRenderer error={error}/>;
                            //IF LOADING
                            if (loading) return <CircleContainerLoader/>;
                            //IF SUCCESS

                            const circles = data.products.items;
                            this.setState({circles});
                            return <CircleContainer
                                circles={this.state.circles}
                                act={this.goToPage}
                                item_num={data.products.total_count}
                                size={size}
                                page={this.state.page}
                            />;
                        }}
                    </Query>

                </section>
            </div>
        );
    }
}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{circles_stats: ({date: Date, city: string, category: string}|ExperiencesList.state.circles_stats|{date, city, category}|Listing.state.circles_stats|{date: null, city: null, category: string}|initialState.circles_stats)}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        circles_stats: state.circles_stats
    }
};

export default connect(mapStateToProps, null)(Home);
