import React, { Component } from "react";
import {CardNumberElement,CardExpiryElement,CardCVCElement, injectStripe} from 'react-stripe-elements';
import '../css/payment.css';
import { connect } from 'react-redux';

class PaymentForm extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    /**
     *
     * @param ev
     * @returns {Promise<void>}
     */
    async submit(ev) {
        ev.preventDefault();
        let {token} = await this.props.stripe.createToken({ name: "Name" });
    }

    /**
     *
     */
    componentDidMount() {
        const handleCard = async name => {
            return await this.props.stripe.createToken({ name: name });
        };
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return (
            <div>
                <h4 className="mb-3">Pagamento</h4>

                <div className="d-block my-3">
                    {
                        (this.props.payments != null)?
                        this.props.payments.map(
                            item => {
                                if (item.code === 'stripecreditcards') {
                                    return (
                                        <div className="custom-control custom-radio">
                                            <input key={"payment-"+item.code} id={item.code} name="paymentMethod" type="radio"
                                                   className="custom-control-input" checked required />
                                            <label className="custom-control-label" htmlFor={item.code}>{item.title}
                                                </label>
                                        </div>
                                    );
                                }
                            }
                        ):""

                    }

                </div>
                <div className="row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="cc-name">Name on card</label>
                        <input type="text" className="form-control" id="cc-name" placeholder="Intestatario Carta"
                               required />
                        <small className="text-muted">Full name as displayed on card</small>
                        <div className="invalid-feedback">
                            Name on card is required
                        </div>
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="cc-number">Credit card number</label>
                        <CardNumberElement />
                        <div className="invalid-feedback">
                            Credit card number is required
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-3 mb-3">
                        <label htmlFor="cc-expiration">Expiration</label>
                        <CardExpiryElement />
                        <div className="invalid-feedback">
                            Expiration date required
                        </div>
                    </div>
                    <div className="col-md-3 mb-3">
                        <label htmlFor="cc-cvv">CVV</label>
                        <CardCVCElement />
                        <div className="invalid-feedback">
                            Security code required
                        </div>
                    </div>
                </div>

            </div>

        )

    }

}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{payment_data: null}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        payment_data: state.payment_data
    }
};

export default connect(mapStateToProps)(PaymentForm);

