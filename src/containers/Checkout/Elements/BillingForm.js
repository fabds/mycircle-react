import React, { Component } from "react";
import { configActions } from '../../../services/Config';

class BillingForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countries: {},
            isLoading: true,
            firstname: this.props.user.firstname,
            lastname: this.props.user.lastname,
            email : this.props.user.email
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidUpdate() {
    }

    componentDidMount() {

        configActions.getCountries().then(

            countries => {

                this.setState({countries: countries,isLoading:false});
            }

        )


    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    render() {

        const { countries, isLoading, firstname, lastname, email } = this.state;

        return (
            <div>
                <div className="row">
                    <div className="col-md-6 mb-3">
                        <label htmlFor="firstName">First name</label>
                        <input name="firstname" type="text" className="form-control" id="firstName" placeholder="Nome" value={firstname} onChange={this.handleChange}
                               required/>
                        <div className="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div className="col-md-6 mb-3">
                        <label htmlFor="lastName">Last name</label>
                        <input name="lastname" type="text" className="form-control" id="lastName" placeholder="Cognome" value={lastname} onChange={this.handleChange}
                               required/>
                        <div className="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                </div>

                {/*<div className="mb-3">*/}
                {/*<label htmlFor="username">Username</label>*/}
                {/*<div className="input-group">*/}
                {/*<div className="input-group-prepend">*/}
                {/*<span className="input-group-text">@</span>*/}
                {/*</div>*/}
                {/*<input type="text" className="form-control" id="username" placeholder="Username"*/}
                {/*required />*/}
                {/*<div className="invalid-feedback" >*/}
                {/*Your username is required.*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*</div>*/}
                {/*<span className="text-muted">(Optional)</span>*/}

                <div className="mb-3">
                    <label htmlFor="email">Email </label>
                    <input name="email" type="email" className="form-control" id="email" placeholder="you@example.com" value={email} onChange={this.handleChange} required/>
                    <div className="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                    </div>
                </div>

                <div className="mb-3">
                    <label htmlFor="address">Address</label>
                    <input type="text" className="form-control" id="address" placeholder="via ..."
                           required/>
                    <div className="invalid-feedback">
                        Please enter your billing address.
                    </div>
                </div>

                <div className="mb-3">
                    <label htmlFor="address2">Indirizzo 2 <span
                        className="text-muted">(Optional)</span></label>
                    <input type="text" className="form-control" id="address2"
                           placeholder="Apartment or suite"/>
                </div>

                <div className="row">
                    <div className="col-md-5 mb-3">
                        <label htmlFor="country">Country</label>
                        <select className="custom-select d-block w-100" id="country" required>
                            <option value="">Choose...</option>
                            <React.Fragment>
                                {!isLoading ?
                                    countries.map(country => <option key={country.id} value={country.id}>{(country.full_name_locale === '') ? country.three_letter_abbreviation : country.full_name_locale}</option>) :
                                    <option value="">Loading...</option>
                                }
                            </React.Fragment>

                        </select>
                        <div className="invalid-feedback">
                            Please select a valid country.
                        </div>
                    </div>
                    <div className="col-md-4 mb-3">
                        <label htmlFor="state">State</label>
                        <select className="custom-select d-block w-100" id="state" required>
                            <option value="">Choose...</option>
                            <option>California</option>
                        </select>
                        <div className="invalid-feedback">
                            Please provide a valid state.
                        </div>
                    </div>
                    <div className="col-md-3 mb-3">
                        <label htmlFor="zip">Zip</label>
                        <input type="text" className="form-control" id="zip" placeholder="" required/>
                        <div className="invalid-feedback">
                            Zip code required.
                        </div>
                    </div>
                </div>
                {/*<hr className="mb-4"></hr>*/}
                {/*<div className="custom-control custom-checkbox">*/}
                {/*<input type="checkbox" className="custom-control-input" id="same-address" />*/}
                {/*<label className="custom-control-label" htmlFor="same-address">Shipping address*/}
                {/*is the same as my billing address</label>*/}
                {/*</div>*/}
                {/*<div className="custom-control custom-checkbox">*/}
                {/*<input type="checkbox" className="custom-control-input" id="save-info" />*/}
                {/*<label className="custom-control-label" htmlFor="save-info">Save this*/}
                {/*information for next time</label>*/}
                {/*</div>*/}
            </div>
        )
    }
}

export default BillingForm