import React, { Component } from "react";
import { userActions } from '../../../services/Users';
import {FormattedMessage} from "react-intl";
import {connect} from "react-redux";

class Cart extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            totals: {},
            isLoading: true,
            cart_data: {}
        };
    }

    /**
     *
     */
    componentDidMount() {

        userActions.getTotals().then(

            totals => {
                this.setState({totals: totals,isLoading:false});
            }

        )

    }

    /**
     *
     * @returns {*}
     */
    render() {

        const { totals, isLoading } = this.state;

        return (
            <div className="col-md-4 order-md-2 mb-4">
                <h4 className="d-flex justify-content-between align-items-center mb-3">
                    <span className="text-muted"><FormattedMessage id="cart.title" defaultMessage="Esperienza" /></span>
                    <span className="badge badge-secondary badge-pill">{(this.props.cart_data !== null)?this.props.cart_data.items_count:""}</span>
                </h4>
                <ul className="list-group mb-3">
                    {(typeof this.props.cart_data !== "undefined" && this.props.cart_data !== null) ?
                        this.props.cart_data.items.map(
                            item =>
                                <li key={"item_cart_"+item.id} className="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 className="my-0">{item.name}</h6>
                                        <small className="text-muted">{item.sku}</small>
                                    </div>
                                    <span className="text-muted">{this.props.cart_data.currency.quote_currency_code} {item.price}</span>
                                </li>
                        ) : <h3>Loading...</h3>
                    }
                    <React.Fragment>
                        {(!isLoading && totals !== null) ?
                            totals.total_segments.map(total =>
                            <li key={total.code} className="list-group-item d-flex justify-content-between">
                                <span><FormattedMessage id="cart.subtotal" defaultMessage={"{title}"} values={{title:total.title}} /> (<FormattedMessage id="cart.currency" defaultMessage={"{currency}"} values={{currency:totals.quote_currency_code}} />)</span>
                                <strong>{total.value}</strong>

                            </li>
                                    ) :
                            <h3>Loading...</h3>
                        }
                    </React.Fragment>
                </ul>

                <form className="card p-2">
                    <div className="input-group">
                        <input type="text" className="form-control" placeholder="Promo code" />
                        <div className="input-group-append">
                            <button type="submit" className="btn btn-secondary">Redeem</button>
                        </div>
                    </div>
                </form>
            </div>
        )

    }

}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{cart_data: ({}|Cart.state.cart_data|Checkout.state.cart_data|null)}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        cart_data: state.checkout_data.cart_data,
    }
};

export default connect(mapStateToProps)(Cart);