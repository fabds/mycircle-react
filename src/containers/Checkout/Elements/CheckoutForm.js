import React, { Component } from "react";
import {userActions} from "../../../services/Users";
import BillingForm from "../Elements/BillingForm";
import PaymentForm from "../Elements/PaymentForm";
import {setPaymentdata} from "../../../store/actions/Payment";
import {connect} from "react-redux";
import {injectStripe} from 'react-stripe-elements';


class CheckoutForm extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isLoadingPayment: true,
            payments_method:{},
            stripe: window.Stripe('pk_test_J0NMmUVULQJFWaYIX0S32hJ0')
            //            stripe: window.Stripe(process.env.STRIPE_KEY)

        };
    }

    /**
     *
     * @param ev
     */
    handleSubmit = (ev) => {
        // We don't want to let default form submission happen here, which would refresh the page.
        ev.preventDefault();

        // Within the context of `Elements`, this call to createToken knows which Element to
        // tokenize, since there's only one in this group.
        this.props.stripe.createToken({name: 'Jenny Rosen'}).then(({token}) => {
            console.log('Received Stripe token:', token);
        });

        // However, this line of code will do the same thing:
        //
        // this.props.stripe.createToken({type: 'card', name: 'Jenny Rosen'});

        // You can also use createSource to create Sources. See our Sources
        // documentation for more: https://stripe.com/docs/stripe-js/reference#stripe-create-source
        //
        // this.props.stripe.createSource({type: 'card', owner: {
        //   name: 'Jenny Rosen'
        // }});
    };

    /**
     *
     */
    logout() {
        userActions.logout();
        window.location.href = '/';
    }

    /**
     *
     */
    componentDidMount() {

        userActions.getPaymentMethods()
            .then(
                payment=> {

                    let methods = (!payment) ? null : payment;
                    this.props.setPaymentdata(methods);
                    this.setState({payments_method:methods,isLoadingPayment: false})
                }
            )
            .catch(
                err => {
                    console.log(err);
                    //this.logout();
                }
            )

    }

    /**
     *
     * @returns {*}
     */
    render() {

        const { isLoadingPayment,payments_method } = this.state;

        return (
            <form className="needs-validation" onSubmit={this.handleSubmit}>
                <BillingForm user={(this.props.cart_data !== null)?this.props.cart_data.customer:""}/>
                <hr className="mb-4"></hr>

                <React.Fragment>
                    {!isLoadingPayment ?   <PaymentForm payments={payments_method} stripe={this.state.stripe}/> : <h3>Loading...</h3> }
                </React.Fragment>
                <hr className="mb-4"></hr>
                <button className="btn btn-primary btn-lg btn-block" type="submit">Continue to
                    checkout
                </button>
            </form>

        );
    }

}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{cart_data: ({}|Checkout.state.cart_data|Cart.state.cart_data|null), payment_data: null}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        cart_data: state.checkout_data.cart_data,
        payment_data: state.checkout_data.payment_data
    }
};

/**
 *
 * @param dispatch
 * @returns {{setPaymentdata: (function(*=): *)}}
 */
function mapDispatchToProps(dispatch) {
    return {
        setPaymentdata: payment_data => dispatch(setPaymentdata(payment_data)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(injectStripe(CheckoutForm));