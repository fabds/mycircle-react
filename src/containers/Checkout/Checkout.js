import React, { PureComponent } from "react";

import { connect } from 'react-redux';
import Cart from "./Elements/Cart";
import {userActions} from "../../services/Users";
import {Elements} from 'react-stripe-elements';
import { setCartdata } from "../../store/actions/Cart";
import CheckoutForm from "./Elements/CheckoutForm";

class Checkout extends PureComponent {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            cart_data: {}

        };
    }

    /**
     *
     */
    componentDidMount() {

      userActions.getCartdata()
            .then(
                cart=> {

                    let cart_data = (!cart) ? null : cart;
                    this.props.setCartdata(cart_data);
                    this.setState({checkout_data:cart_data,isLoading: false})
                }
            )
          .catch(
              err => {
                  console.log(err);
                  //this.logout();
              }
          )

    }

    /**
     *
     */
    logout() {
        userActions.logout();
        window.location.href = '/';
    }

    /**
     *
     * @returns {*}
     */
    render() {

        const { isLoading,cart_data } = this.state;

        return (

                <section className="popular-destination-area section-gap">
                    <div className="clearfix separator"></div>
                    <div className="clearfix separator"></div>
                    <div className="clearfix separator"></div>
                    <div className="container">
                        <div className="py-5 text-center">
                              <h2>Checkout form</h2>
                        </div>

                        <div className="row">
                            <React.Fragment>
                                {!isLoading ?   <Cart cart_data={cart_data}/> : <h3>Loading...</h3> }
                            </React.Fragment>
                            <div className="col-md-8 order-md-1">
                                <h4 className="mb-3">Billing address</h4>
                                <React.Fragment>
                                    <Elements>
                                    {!isLoading ?
                                        <CheckoutForm cart_data={cart_data}/> :
                                        <h3>Loading...</h3> }
                                    </Elements>
                                </React.Fragment>
                            </div>
                        </div>
                        <hr className="mb-4"></hr>
                    </div>
                </section>


    );
    }
}

//ownProps.date_start === state.date_start
/**
 *
 * @param state
 * @param ownProps
 * @returns {{cart_data: ({}|Checkout.state.cart_data|Cart.state.cart_data|null)}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        cart_data: state.cart_data,
    }
};

/**
 *
 * @param dispatch
 * @returns {{setCartdata: (function(*=): *)}}
 */
function mapDispatchToProps(dispatch) {
    return {
        setCartdata: cart_data => dispatch(setCartdata(cart_data)),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(Checkout);
