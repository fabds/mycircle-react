import React, { PureComponent } from "react";

import ExperiencesList from "./ExperiencesList";
import { Redirect } from 'react-router-dom'
import ErrorRenderer from "../../components/UI/Errors/ErrorRenderer";
import FullScreenLoader from "../../components/UI/Loaders/FullScreenLoader/FullScreenLoader";
import {Query} from "react-apollo";
import { URLS } from '../../graphql/urls';

class ExperiencesMain extends PureComponent {

    /**
     *
     * @returns {*}
     */
    render() {

        let current_url = this.props.match.params.category;

        return (

            <Query query={URLS}
                   variables={{url:current_url}} >
                {({ data, error, loading }) => {
                    //IF ERROR
                    if (error) return <ErrorRenderer error={error}/>;
                    //IF LOADING
                    if (loading) return <FullScreenLoader />;
                    //IF SUCCESS

                    if (data.urlResolver.type === 'CATEGORY')
                        return  <ExperiencesList category={data.urlResolver} {...this.props} />;
                    else
                        return <Redirect to='/404' />;

                }}
            </Query>


        );
    }
}


export default ExperiencesMain;
