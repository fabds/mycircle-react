import React, { PureComponent } from "react";

import CircleSearch from "../../components/Elements/Forms/CircleSearch";
import { connect } from 'react-redux';
import { Query } from 'react-apollo';

import { CIRCLES } from '../../graphql/circles';
import SectionTitle from "../../components/UI/Titles/SectionTitles";
import BlockSeparator from "../../components/UI/Separators/BlockSeparator";
import BannerContent from "../../components/Content/BannerContent/BannerContent";
import ErrorRenderer from "../../components/UI/Errors/ErrorRenderer";
import FullScreenLoader from "../../components/UI/Loaders/FullScreenLoader/FullScreenLoader";
import CircleSlider from "../../components/Content/CircleSlider/CircleSlider";

class ExperiencesList extends PureComponent {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            circles: [],
            circles_slider: [],
            loading: false,
            badges: [],
            size: process.env.REACT_APP_ELEMENT_PER_PAGE,
            recurrent: 0,
            page: 1,
            sizeSlider: process.env.REACT_APP_SLIDER_ELEMENTS,

        };
    }

    /**
     *
     * @param $
     */
    initCircleCounter($) {
        $('body .countdown').each(function (i, v) {
            $(this).circleTimer({ debug: false });
        });
    }

    /**
     *
     * @param $
     */
    initCircleStars($) {
        $('.stars-elements').empty();
        $('.stars-elements').each(function (i, v) {
            $(this).stars();
        });
    }

    /**
     *
     */
    componentDidUpdate() {
        this.initCircleStars(window.jQuery);
        this.initCircleCounter(window.jQuery);
        window.adjustHeader();
    }

    /**
     *
     * @returns {*}
     */
    render() {

        var variables = {};

        let current_cat = this.props.category.id;
        if (this.props.circles_stats.date) variables.date = new Date(this.props.circles_stats.date).toISOString().slice(0, 10);
        if (this.props.circles_stats.city) variables.city = this.props.circles_stats.city;
        if (current_cat) variables.category = current_cat;
        if (this.props.circles_stats.recurrent) variables.recurrent = this.props.circles_stats.recurrent;
        variables.size = this.state.sizeSlider;
        variables.page = this.state.page;
        //var page = this.state.page;
        //var size = this.state.size;

        return (
            <div>

                <section className="popular-destination-area section-gap">
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />
                    <BlockSeparator />


                    <Query query={CIRCLES}
                           variables={variables}>
                        {({data, error, loading}) => {
                            //IF ERROR
                            if (error) return <ErrorRenderer error={error}/>;
                            //IF LOADING
                            if (loading) return <FullScreenLoader/>;
                            //IF SUCCESS
                            const circles_slider = data.products.items;
                            this.setState({filters:data.products.filters});
                            return <div>
                                        <CircleSearch filters={data.products.filters} current_category={current_cat} />
                                        <BlockSeparator />
                                        <CircleSlider circles={circles_slider}/>
                                    </div>;
                        }}
                    </Query>

                    <BlockSeparator />

                    <SectionTitle title="Potrebbe Interessarti"/>

                    <BannerContent />

                </section>
            </div>
        );
    }
}

/**
 *
 * @param state
 * @param ownProps
 * @returns {{circles_stats: ({date: Date, city: string, category: string}|ExperiencesList.state.circles_stats|{date, city, category}|Listing.state.circles_stats|{date: null, city: null, category: string}|initialState.circles_stats), categories: (Array|string|string[])}}
 */
const mapStateToProps = (state, ownProps) => {
    return {
        circles_stats: state.circles_stats,
        categories: state.categories
    }
};

export default connect(mapStateToProps)(ExperiencesList);
