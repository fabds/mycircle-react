import React, {Component} from "react";
import {connect} from "react-redux";
import Gravatar from 'react-gravatar';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';

class Account extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    /**
     *
     * @param tab
     */
    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    /**
     *
     * @returns {*}
     */
    render() {
        return (
            <section className="popular-destination-area section-gap">
                <div className="clearfix separator"></div>
                <div className="clearfix separator"></div>
                <div className="clearfix separator"></div>
                <div className="container">
                    <div className="row">
                        <div className="clearfix separator"></div>
                        <div className="col-md-4 text-center">
                            <Gravatar email={this.props.user.email} size={150} className="rounded-circle" default="mp"/>
                            <br/><br/>
                            <h3>{this.props.user.firstname+' '+this.props.user.lastname}</h3>
                            <h5><i className="fas fa-envelope"></i> {this.props.user.email}</h5>
                        </div>
                        <div className="col-md-8">
                            <div>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: this.state.activeTab === '1' })}
                                            onClick={() => { this.toggle('1'); }}
                                        >
                                            Profilo
                                        </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: this.state.activeTab === '2' })}
                                            onClick={() => { this.toggle('2'); }}
                                        >
                                            Ordini
                                        </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={this.state.activeTab}>
                                    <TabPane tabId="1">
                                        <Row>
                                            <Col sm="12">
                                                <br/>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br/><br/>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <table id="cart" className="table table-hover table-condensed">
                                            <thead>
                                            <tr>
                                                <th style={{"width": "50%"}}>Cerchia</th>
                                                <th style={{"width": "10%"}}>Prezzo</th>
                                                <th style={{"width": "8%"}}>Quantità</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td data-th="Product">
                                                    <div className="row">
                                                        <div className="col-sm-3 hidden-xs text-center">
                                                            <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <h4 className="nomargin">Circle 3</h4>
                                                            <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-th="Price">€ 1.99</td>
                                                <td data-th="Quantity">
                                                    1
                                                </td>

                                            </tr>

                                            <tr>
                                                <td data-th="Product">
                                                    <div className="row">
                                                        <div className="col-sm-3 hidden-xs text-center">
                                                            <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <h4 className="nomargin">Circle 3</h4>
                                                            <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-th="Price">€ 1.99</td>
                                                <td data-th="Quantity">
                                                    1
                                                </td>

                                            </tr>

                                            <tr>
                                                <td data-th="Product">
                                                    <div className="row">
                                                        <div className="col-sm-3 hidden-xs text-center">
                                                            <img src="http://placehold.it/100x100" alt="..." className="img-responsive rounded-circle"/>
                                                        </div>
                                                        <div className="col-sm-9">
                                                            <h4 className="nomargin">Circle 3</h4>
                                                            <p>Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet.</p>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-th="Price">€ 1.99</td>
                                                <td data-th="Quantity">
                                                    1
                                                </td>

                                            </tr>
                                            </tbody>

                                        </table>
                                        <Row>
                                            <Col sm="6">
                                                <br/>
                                                <Card body>
                                                    <CardTitle>Special Title Treatment</CardTitle>
                                                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                                    <Button>Go somewhere</Button>
                                                </Card>
                                            </Col>
                                            <Col sm="6">
                                                <br/>
                                                <Card body>
                                                    <CardTitle>Special Title Treatment</CardTitle>
                                                    <CardText>With supporting text below as a natural lead-in to additional content.</CardText>
                                                    <Button>Go somewhere</Button>
                                                </Card>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                </TabContent>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

/**
 *
 * @param state
 * @returns {{user: *, authenticated: boolean}}
 */
const mapState = (state) => ({
    user: state.session.user,
    authenticated: state.session.authenticated
});

export default connect(mapState)(Account);