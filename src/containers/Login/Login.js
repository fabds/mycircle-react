import React, {Component} from "react"
import { SESSION } from '../../store/types'
import { userActions } from '../../services/Users';

class Login extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            submitted: false,
            login: false,
            loginmsg: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        const session = localStorage.getItem(SESSION)
        if(session){
            this.props.history.push(`/`);
        }
    }

    /**
     *
     * @param e
     */
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    /**
     *
     * @param e
     */
    handleSubmit(e) {
        e.preventDefault();

        const { username, password } = this.state;

        if (username && password) {

            userActions.login(username, password).then(
                user=> {
                    if (user == null) {
                        this.setState({loginmsg:'Errore username o password'})
                    } else {
                        this.setState({loginmsg:'Login effettuato'})
                    }
                }
            ).catch(
                message => {
                    this.setState({loginmsg:JSON.stringify({message})})
                })

        }

        this.setState({ submitted: true });

    }

    /**
     *
     * @returns {*}
     */
    render() {
        const { username, password, submitted } = this.state;
        return (
            <section className="popular-destination-area section-gap">
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
                <div className="container">
                    <div className="row text-center">
                        <div className="col-md-3">
                            &nbsp;
                        </div>
                        <div className="col-md-6">
                            <h1 className="text-center">Login</h1>
                            <br/><br/>
                            <form id="Login" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input autoComplete="username" type="email" className="form-control" name="username" placeholder="Email Address" value={username}  onChange={this.handleChange}  />
                                    {submitted && !username &&
                                        <div className="help-block">Username is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input autoComplete="current-password" type="password" className="form-control" name="password" placeholder="Password" value={password} onChange={this.handleChange}  />
                                    {submitted && !password &&
                                        <div className="help-block">Password is required</div>
                                    }
                                </div>
                                
                                <br/>
                                
                                <button type="submit" className="btn btn-primary">Login</button>

                            </form>
                            <br/>
                            <div className="message">{this.state.loginmsg}</div>
                            
                        </div>
                    </div>
                </div>
            </section>
            );
    }
    
}

export default Login