import React, {Component} from "react"
import { SESSION } from '../../store/types'
import { userActions } from '../../services/Users';

class Register extends Component {

    /**
     *
     * @param props
     */
    constructor(props) {
        super(props);

        this.state = {
            nickname: '',
            firstname: '',
            lastname: '',
            username: '',
            password: '',
            confirm_password: '',
            submitted: false,
            login: false,
            registermsg: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        const session = localStorage.getItem(SESSION)
        if(session){
            this.props.history.push(`/`);
        }
    }

    /**
     *
     * @param e
     */
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    /**
     *
     * @param e
     */
    handleSubmit(e) {
        e.preventDefault();

        const { nickname, firstname, lastname, username, password, confirm_password } = this.state;

        if ((nickname && firstname && lastname && username && password) && (password === confirm_password)) {

            userActions.register(nickname, firstname, lastname, username, password, confirm_password).then(
                user=> {
                    this.setState({registermsg:'Registrazione effettuata'})
                }
            ).catch(
                res => {
                    this.setState({registermsg:res.toString()})
                })

        }

        this.setState({ submitted: true });

    }

    /**
     *
     * @returns {*}
     */
    render() {
        const { nickname, firstname, lastname, username, password, confirm_password, submitted } = this.state;
        return (
            <section className="popular-destination-area section-gap">
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
            <div className="clearfix separator"></div>
                <div className="container">
                    <div className="row text-center">
                        <div className="col-md-3">
                            &nbsp;
                        </div>
                        <div className="col-md-6">
                            <h1 className="text-center">Registrazione</h1>
                            <br/><br/>
                            <form id="Register" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <input autoComplete="firstname" type="text" className="form-control" name="firstname" placeholder="Nome" value={firstname}  onChange={this.handleChange}  />
                                    {submitted && !firstname &&
                                    <div className="help-block">Nome is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input autoComplete="lastname" type="text" className="form-control" name="lastname" placeholder="Cognome" value={lastname}  onChange={this.handleChange}  />
                                    {submitted && !lastname &&
                                    <div className="help-block">Cognome is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input autoComplete="nickname" type="text" className="form-control" name="nickname" placeholder="Nickname" value={nickname}  onChange={this.handleChange}  />
                                    {submitted && !nickname &&
                                    <div className="help-block">Nickname is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input autoComplete="username" type="email" className="form-control" name="username" placeholder="Email Address" value={username}  onChange={this.handleChange}  />
                                    {submitted && !username &&
                                        <div className="help-block">Username is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input autoComplete="current-password" type="password" className="form-control" name="password" placeholder="Password" value={password} onChange={this.handleChange}  />
                                    {submitted && !password &&
                                        <div className="help-block">Password is required</div>
                                    }
                                </div>

                                <div className="form-group">
                                    <input type="password" className="form-control" name="confirm_password" placeholder="Conferma Password" value={confirm_password} onChange={this.handleChange}  />
                                    {submitted && (password !== confirm_password) &&
                                    <div className="help-block">Password match</div>
                                    }
                                </div>
                                
                                <br/>
                                
                                <button type="submit" className="btn btn-primary">Registrati</button>

                            </form>
                            <br/>
                            <div className="message">{this.state.registermsg}</div>
                            
                        </div>
                    </div>
                </div>
            </section>
            );
    }
    
}

export default Register