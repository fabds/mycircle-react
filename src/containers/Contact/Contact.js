import React from "react"
import BlockSeparator from "../../components/UI/Separators/BlockSeparator";
import imgdemo from "../../assets/img/d1.jpg";

/**
 *
 * @param props
 * @returns {*}
 * @constructor
 */
const Contact = (props) => {
    return (
    <section className="popular-destination-area section-gap">
    <div className="clearfix separator"></div>
    <div className="clearfix separator"></div>
    <div className="clearfix separator"></div>
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <h1 className="text-center">Contatti</h1>
                </div>
                <BlockSeparator/>
                <div className="col-md-6">
                    <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p className="text-justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                </div>
                <div className="col-md-6">
                    <img src={imgdemo} alt="img" className="img-fluid" style={{"width":"100%"}} />
                    <br/><br/>
                    <form name="sentMessage" id="contactForm" noValidate="">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Nome *" id="name"
                                           required="" data-validation-required-message="Inserisci il tuo nome."/>
                                        <p className="help-block text-danger"></p>
                                </div>

                                <div className="form-group">
                                    <input type="tel" className="form-control" placeholder="Telefono *" id="phone"
                                           required=""
                                           data-validation-required-message="Inserisci il tuo numero di telefono."/>
                                        <p className="help-block text-danger"></p>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="Cognome *" id="name"
                                           required="" data-validation-required-message="Inserisci il tuo cognome."/>
                                    <p className="help-block text-danger"></p>
                                </div>

                                <div className="form-group">
                                    <input type="email" className="form-control" placeholder="Email *" id="email"
                                           required=""
                                           data-validation-required-message="Inserisci la tua email."/>
                                    <p className="help-block text-danger"></p>
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group">
                                    <textarea className="form-control" placeholder="Messaggio *" id="message"
                                              required=""
                                              data-validation-required-message="Inserisci messaggio."></textarea>
                                    <p className="help-block text-danger"></p>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                            <div className="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" className="btn btn-lg get btn-primary">Invia Messaggio</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    )
};

export default Contact